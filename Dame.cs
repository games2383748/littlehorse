using System;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;

public class Dame {

    private Image PionBlanc = Image.FromFile("img/Dame/PionBlanc.png");
    private Image PionNoir = Image.FromFile("img/Dame/PionNoir.png");
    private Image DameBlanc = Image.FromFile("img/Dame/DameBlanc.png");
    private Image DameNoir = Image.FromFile("img/Dame/DameNoir.png");

    private API window;
    private Panel GameP;
    private Button[,] Plateau;
    private Button[,] Pions;
    private Button[,] Dames;
    private Button jouer;
    private Button more;
    private Panel More;
    private Button setting;
    private Button exit;
    private int player;
    private Button saver;
    private Button[] trajet;
    private Button[,] Dametrajet;
    private bool again=false;

    //Contsante
    private Color ColorParam = Color.LightGray;
    private Color ColorParamButton = Color.Gray;



    public Dame(API window) {
        this.window = window;

        this.player = 1;
        this.GameP = new Panel();
        this.Plateau = new Button[10,10];
        this.Pions = new Button[2,20];

        Init();
    }




    private void Init() {
        this.jouer = new Button();
        this.more = new Button();
        this.trajet = new Button[2];
        this.Dametrajet = new Button[4,9];
        this.Dames = new Button[2,20];

        this.GameP.Controls.Add(this.jouer);
        this.GameP.Controls.Add(this.more);

        int j=0;


        Size CaseSize = new Size(this.window.GameWindow.Width/20,this.window.GameWindow.Width/20);

        this.GameP.Size = this.window.GameWindow.Size;
        this.GameP.BackColor = Color.LightGray;



        for(int i=0;i<10;i++) {
            for(j=0;j<10;j++) {
                this.Plateau[i,j] = new Button();
                this.GameP.Controls.Add(this.Plateau[i,j]);
                this.Plateau[i,j].Size = CaseSize;
                this.Plateau[i,j].Enabled = false;
                this.Plateau[i,j].FlatStyle = FlatStyle.Flat;
                this.Plateau[i,j].FlatAppearance.BorderSize = 0;
                if(i%2 == 0) {
                    if(j%2 == 0) {
                        this.Plateau[i,j].BackColor = Color.Beige;
                    }
                    else {
                        this.Plateau[i,j].BackColor = Color.Brown;
                    }
                        
                }
                else {
                    if(j%2 == 0) {
                        this.Plateau[i,j].BackColor = Color.Brown;
                    }
                    else {
                        this.Plateau[i,j].BackColor = Color.Beige;
                    }
                }
            }
        }

        
        this.Plateau[0,0].Location = new Point(this.GameP.Width/2-this.Plateau[0,0].Width*5, this.GameP.Height/2-this.Plateau[0,0].Height*5);
        for(int i=0;i<10;i++) {
            if(i!=0) {
                this.Plateau[i,0].Location = new Point(this.Plateau[i-1,0].Left, this.Plateau[i-1,0].Bottom);
            }
            for(j=1;j<10;j++) {
                this.Plateau[i,j].Location = new Point(this.Plateau[i,j-1].Right, this.Plateau[i,j-1].Top);
            }
        }

        int y=0;
        int x=0;
        for(int i=0;i<20;i++) {
            this.Pions[0,i] = new Button();
            while(this.Plateau[y,x].BackColor != Color.Brown) {
                x++;
                if(x>9) {
                    x=0;
                    y++;
                }
            }
            this.Plateau[y,x].Controls.Add(this.Pions[0,i]);
            x++;
            if(x>9) {
                x=0;
                y++;
            }

            this.Pions[0,i].Size = CaseSize;
            this.Pions[0,i].BackColor = Color.Transparent;
            this.Pions[0,i].Enabled = false;
            this.Pions[0,i].FlatStyle = FlatStyle.Flat;
            this.Pions[0,i].FlatAppearance.BorderSize = 0;
            this.Pions[0,i].BackgroundImage = this.PionNoir;
            this.Pions[0,i].BackgroundImageLayout = ImageLayout.Zoom;

            this.Dames[0,i] = new Button();
            this.Dames[0,i].Size = CaseSize;
            this.Dames[0,i].BackColor = Color.Transparent;
            this.Dames[0,i].Enabled = false;
            this.Dames[0,i].FlatStyle = FlatStyle.Flat;
            this.Dames[0,i].FlatAppearance.BorderSize = 0;
            this.Dames[0,i].BackgroundImage = this.DameNoir;
            this.Dames[0,i].BackgroundImageLayout = ImageLayout.Zoom;
        }

        y=6;
        x=0;
        for(int i=0;i<20;i++) {
            this.Pions[1,i] = new Button();
            while(this.Plateau[y,x].BackColor != Color.Brown) {
                x++;
                if(x>9) {
                    x=0;
                    y++;
                }
            }
            this.Plateau[y,x].Controls.Add(this.Pions[1,i]);
            x++;
            if(x>9) {
                x=0;
                y++;
            }

            this.Pions[1,i].Size = CaseSize;
            this.Pions[1,i].BackColor = Color.Transparent;
            this.Pions[1,i].Enabled = false;
            this.Pions[1,i].FlatStyle = FlatStyle.Flat;
            this.Pions[1,i].FlatAppearance.BorderSize = 0;
            this.Pions[1,i].BackgroundImage = this.PionBlanc;
            this.Pions[1,i].BackgroundImageLayout = ImageLayout.Zoom;

            this.Dames[1,i] = new Button();
            this.Dames[1,i].Size = CaseSize;
            this.Dames[1,i].BackColor = Color.Transparent;
            this.Dames[1,i].Enabled = false;
            this.Dames[1,i].FlatStyle = FlatStyle.Flat;
            this.Dames[1,i].FlatAppearance.BorderSize = 0;
            this.Dames[1,i].BackgroundImage = this.DameBlanc;
            this.Dames[1,i].BackgroundImageLayout = ImageLayout.Zoom;
        }

        this.jouer.Size = new Size(Plateau[0,0].Width*2,Plateau[0,0].Height);
        this.jouer.Location = new Point(this.Plateau[9,9].Right+this.jouer.Width, this.Plateau[9,9].Top);
        this.jouer.Text = "PLAY";
        this.jouer.BackColor = Color.Gray;
        this.jouer.FlatStyle = FlatStyle.Flat;
        this.jouer.FlatAppearance.BorderSize = this.jouer.Width/10;
        this.jouer.FlatAppearance.BorderColor = Color.LightGray;
        this.jouer.Font = new Font(this.jouer.Font.Name, this.jouer.Width/10);
        this.jouer.Click += new EventHandler(jouer_Click);


        Init_More();
    }

    private void Init_More() {
        this.exit = new Button();
        this.setting = new Button();
        this.More = new Panel();

        this.More.Controls.Add(this.exit);
        this.More.Controls.Add(this.setting);

        this.More.Size = new Size(this.more.Width*4, this.more.Height*2);
        this.More.Location = new Point(this.more.Right-this.More.Width,(int)(this.more.Bottom*1.1));
        this.More.BackColor = ColorParamButton;
        
        
        this.setting.Text = "Paramètres";
        this.setting.Size = new Size(this.More.Width,this.More.Height/2);
        this.setting.FlatStyle = FlatStyle.Flat;
        this.setting.FlatAppearance.BorderSize = 0;
        this.setting.FlatAppearance.MouseOverBackColor = this.exit.FlatAppearance.CheckedBackColor;
        this.setting.BackColor = ColorParam;
        this.setting.Click += new EventHandler(setting_Click);


        this.exit.Text = "Retourner Au Menu";
        this.exit.Size = new Size(this.More.Width,this.More.Height/2);
        this.exit.Location = new Point(this.setting.Left,this.setting.Bottom);
        this.exit.FlatStyle = FlatStyle.Flat;
        this.exit.FlatAppearance.BorderSize = 0;
        this.exit.FlatAppearance.MouseOverBackColor = this.exit.FlatAppearance.CheckedBackColor;
        this.exit.BackColor = ColorParam;
        this.exit.Click += new EventHandler(Menu_Return);
    }




    private void Menu_Return(object sender, EventArgs e) {
        this.GameP.Controls.Remove(this.More);
        this.more.Enabled = true;
        this.window.GameWindow.Controls.Remove(this.GameP);
        this.window.Home();
    }
    private void setting_Click(object sender, EventArgs e) {
        this.window.settingWindow.add();
        this.window.settingWindow.Resize_Setting();
    }
    private void more_Click(object sender, EventArgs e) {
        this.More.Location = new Point(this.more.Right-this.More.Width,this.more.Bottom+this.more.Height/20);
        this.GameP.Controls.Add(this.More);
        this.more.Enabled = false;
    }
    private void jouer_Click(object sender, EventArgs e) {
        for(int i=0;i<20;i++) {
            this.Pions[this.player,i].Parent.Click += new EventHandler(ClickPion);
            for(int y=0;y<10;y++) {
                for(int x=0;x<10;x++) {
                    if(this.Pions[this.player,i].Parent == this.Plateau[y,x]) {
                        Verify(y,x);
                    }
                }
            }
        }
        this.GameP.Controls.Remove(this.jouer);
        this.jouer.Click -= new EventHandler(jouer_Click);
    }

    private void CanMoveLeft(object sender, EventArgs e) {
        int y=-1,x=-1;
        if(this.saver != null) {
            this.saver.Enabled = true;
        }
        this.saver = (Button)sender;
        this.saver.Enabled = false;

        for(int i=0;i<10;i++) {
            for(int j=0;j<10;j++) {
                if(this.saver == this.Plateau[i,j]) {
                    y=i;
                    x=j;
                }
            }
        }
        if(this.player == 0) {
            this.Plateau[y+1,x-1].Enabled = true;
            this.Plateau[y+1,x-1].Click += new EventHandler(GoHere);
            this.trajet[0] = this.Plateau[y+1,x-1];
        }
        else {
            this.Plateau[y-1,x-1].Enabled = true;
            this.Plateau[y-1,x-1].Click += new EventHandler(GoHere);
            this.trajet[0] = this.Plateau[y-1,x-1];
        }
        
    }
    private void CanMoveRight(object sender, EventArgs e) {
        int y=-1,x=-1;
        if(this.saver != null) {
            this.saver.Enabled = true;
        }
        this.saver = (Button)sender;
        this.saver.Enabled = false;

        for(int i=0;i<10;i++) {
            for(int j=0;j<10;j++) {
                if(this.saver == this.Plateau[i,j]) {
                    y=i;
                    x=j;
                }
            }
        }
        if(this.player == 0) {
            this.Plateau[y+1,x+1].Enabled = true;
            this.Plateau[y+1,x+1].Click += new EventHandler(GoHere);
            this.trajet[1] = this.Plateau[y+1,x+1];
        }
        else {
            this.Plateau[y-1,x+1].Enabled = true;
            this.Plateau[y-1,x+1].Click += new EventHandler(GoHere);
            this.trajet[1] = this.Plateau[y-1,x+1];
        }
    }
    private void CanEatLeft(object sender, EventArgs e) {
        int y=-1,x=-1;
        if(this.saver != null) {
            this.saver.Enabled = true;
        }
        this.saver = (Button)sender;
        this.saver.Enabled = false;

        for(int i=0;i<10;i++) {
            for(int j=0;j<10;j++) {
                if(this.saver == this.Plateau[i,j]) {
                    y=i;
                    x=j;
                }
            }
        }
        if(this.player == 0) {
            this.Plateau[y+2,x-2].Enabled = true;
            this.Plateau[y+2,x-2].Click += new EventHandler(GoAndEatLeft);
            this.trajet[0] = this.Plateau[y+2,x-2];
        }
        else {
            this.Plateau[y-2,x-2].Enabled = true;
            this.Plateau[y-2,x-2].Click += new EventHandler(GoAndEatLeft);
            this.trajet[0] = this.Plateau[y-2,x-2];
        }
    }
    private void CanEatRight(object sender, EventArgs e) {
        int y=-1,x=-1;
        if(this.saver != null) {
            this.saver.Enabled = true;
        }
        this.saver = (Button)sender;
        this.saver.Enabled = false;

        for(int i=0;i<10;i++) {
            for(int j=0;j<10;j++) {
                if(this.saver == this.Plateau[i,j]) {
                    y=i;
                    x=j;
                }
            }
        }
        if(this.player == 0) {
            this.Plateau[y+2,x+2].Enabled = true;
            this.Plateau[y+2,x+2].Click += new EventHandler(GoAndEatRight);
            this.trajet[1] = this.Plateau[y+2,x+2];
        }
        else {
            this.Plateau[y-2,x+2].Enabled = true;
            this.Plateau[y-2,x+2].Click += new EventHandler(GoAndEatRight);
            this.trajet[1] = this.Plateau[y-2,x+2];
        }
    }

    private void ClickPion(object sender, EventArgs e) {
        int n=0;
        if(this.trajet[0] != null) {
            this.trajet[0].Enabled = false;
            this.trajet[0].Click -= new EventHandler(GoHere);
        }
        if(this.trajet[1] != null) {
            this.trajet[1].Enabled = false;
            this.trajet[1].Click -= new EventHandler(GoHere);
        }
        for(int i=0;i<4;i++) {
            while(this.Dametrajet[i,n] != null) {
                this.Dametrajet[i,n].Enabled = false;
                this.Dametrajet[i,n].Click -= new EventHandler(GoHere);
            }
        }
        
    }


    private void GoHere(object sender, EventArgs e) {
        int y=-1,n=-1;
        Button Case = (Button)sender;
        Button pion = (Button)(this.saver.GetChildAtPoint(new Point(0,0)));
        this.saver.Controls.Remove(pion);
        Case.Controls.Add(pion);

        for(int i=0;i<10;i++) {
            for(int j=0;j<10;j++) {
                if(Case == this.Plateau[i,j]) {
                    y=i;
                    break;
                }
            }
        }
        for(int i=0;i<20;i++) {
            if(pion == this.Pions[this.player,i]) {
                n=i;
                break;
            }
        }

        if(this.player == 0) {
            if(y == 9) {
                Case.Controls.Remove(pion);
                Case.Controls.Add(this.Dames[0,n]);
            }
        }
        else {
            if(y == 0) {
                Case.Controls.Remove(pion);
                Case.Controls.Add(this.Dames[1,n]);
            }
        }

        NextPlayer();
    }


    private void GoAndEatLeft(object sender, EventArgs e) {
        int n=-1;
        int x=-1,y=-1;
        Button Case = (Button)sender;
        Button pion = (Button)(this.saver.GetChildAtPoint(new Point(0,0)));
        this.saver.Controls.Remove(pion);
        Case.Controls.Add(pion);

        if(this.player==0) {
            for(int i=0;i<10;i++) {
                for(int j=0;j<10;j++) {
                    if(this.Plateau[i,j] == Case) {
                        y = i;
                        x = j;

                        this.Plateau[y-1,x+1].Controls.Remove(this.Plateau[y-1,x+1].GetChildAtPoint(new Point(0,0)));
                        
                        break;
                    }
                }
            }
        }
        else {
            for(int i=0;i<10;i++) {
                for(int j=0;j<10;j++) {
                    if(this.Plateau[i,j] == Case) {
                        y = i;
                        x = j;

                        this.Plateau[y+1,x+1].Controls.Remove(this.Plateau[y+1,x+1].GetChildAtPoint(new Point(0,0)));
                        
                        break;
                    }
                }
            }
        }
        for(int i=0;i<20;i++) {
            if(pion == this.Pions[this.player,i]) {
                n=i;
                break;
            }
        }

        if(this.player == 0) {
            if(y == 9) {
                Case.Controls.Remove(pion);
                Case.Controls.Add(this.Dames[0,n]);
            }
        }
        else if(y == 0) {
            Case.Controls.Remove(pion);
            Case.Controls.Add(this.Dames[1,n]);
        }
        CanEat(y,x);

        if(this.again == false) {
            NextPlayer();
        }
        this.again = false;

        
    }
    private void GoAndEatRight(object sender, EventArgs e) {
        int n=-1;
        int x=-1,y=-1;
        Button Case = (Button)sender;
        Button pion = (Button)(this.saver.GetChildAtPoint(new Point(0,0)));
        this.saver.Controls.Remove(pion);
        Case.Controls.Add(pion);
        
        for(int i=0;i<10;i++) {
            for(int j=0;j<10;j++) {
                if(this.Plateau[i,j] == Case) {
                    y = i;
                    x = j;
                }
            }
        }

        if(this.player==0) {
            this.Plateau[y-1,x-1].Controls.Remove(this.Plateau[y-1,x-1].GetChildAtPoint(new Point(0,0)));
        }
        else {
            this.Plateau[y+1,x-1].Controls.Remove(this.Plateau[y+1,x-1].GetChildAtPoint(new Point(0,0)));
        }

        if(this.player == 0) {
            if(y == 9) {
                Case.Controls.Remove(pion);
                Case.Controls.Add(this.Dames[0,n]);
            }
        }
        else if(y == 0) {
            Case.Controls.Remove(pion);
            Case.Controls.Add(this.Dames[1,n]);
        }
        CanEat(y,x);

        if(this.again == false) {
            NextPlayer();
        }
        this.again = false;
    }


    private void CanMoveDownLeft(object sender, EventArgs e) {
        int x=-1,y=-1;
        Button Case = (Button)sender;
        if(this.saver != null) {
            this.saver.Enabled = true;
        }
        this.saver = Case;

        for(int i=0;i<10;i++) {
            for(int j=0;j<10;j++) {
                if(Case == this.Plateau[i,j]) {
                    y=i;
                    x=j;
                    break;
                }
            }
        }

        while(y<9 && x>0) {
            y++;
            x--;
            if(this.Plateau[y,x].GetChildAtPoint(new Point(0,0)) == null) {
                this.Plateau[y,x].Enabled = true;
                this.Plateau[y,x].Click += new EventHandler(GoHere);
                this.Dametrajet[0,x] = this.Plateau[y,x];
            }
            else {
                break;
            }
        }
    }
    private void CanMoveUpLeft(object sender, EventArgs e) {
        int x=-1,y=-1;
        Button Case = (Button)sender;
        if(this.saver != null) {
            this.saver.Enabled = true;
        }
        this.saver = Case;

        for(int i=0;i<10;i++) {
            for(int j=0;j<10;j++) {
                if(Case == this.Plateau[i,j]) {
                    y=i;
                    x=j;
                    break;
                }
            }
        }

        while(y>0 && x>0) {
            y--;
            x--;
            if(this.Plateau[y,x].GetChildAtPoint(new Point(0,0)) == null) {
                this.Plateau[y,x].Enabled = true;
                this.Plateau[y,x].Click += new EventHandler(GoHere);
                this.Dametrajet[1,x] = this.Plateau[y,x];
            }
            else {
                break;
            }
            
        }
    }
    private void CanMoveDownRight(object sender, EventArgs e) {
        int x=-1,y=-1;
        Button Case = (Button)sender;
        if(this.saver != null) {
            this.saver.Enabled = true;
        }
        this.saver = Case;

        for(int i=0;i<10;i++) {
            for(int j=0;j<10;j++) {
                if(Case == this.Plateau[i,j]) {
                    y=i;
                    x=j;
                    break;
                }
            }
        }

        while(y<9 && x<9) {
            y++;
            x++;
            if(this.Plateau[y,x].GetChildAtPoint(new Point(0,0)) == null) {
                this.Plateau[y,x].Enabled = true;
                this.Plateau[y,x].Click += new EventHandler(GoHere);
                this.Dametrajet[2,x] = this.Plateau[y,x];
            }
            else {
                break;
            }
            
        }
    }
    private void CanMoveUpRight(object sender, EventArgs e) {
        int x=-1,y=-1;
        Button Case = (Button)sender;
        if(this.saver != null) {
            this.saver.Enabled = true;
        }
        this.saver = Case;

        for(int i=0;i<10;i++) {
            for(int j=0;j<10;j++) {
                if(Case == this.Plateau[i,j]) {
                    y=i;
                    x=j;
                    break;
                }
            }
        }

        while(y>0 && x<9) {
            y--;
            x++;
            if(this.Plateau[y,x].GetChildAtPoint(new Point(0,0)) == null) {
                this.Plateau[y,x].Enabled = true;
                this.Plateau[y,x].Click += new EventHandler(GoHere);
                this.Dametrajet[3,x] = this.Plateau[y,x];
            }
            else {
                break;
            }
            
        }
    }

    private void CanEatDownLeft(object sender, EventArgs e) {

    }
    private void CanEatUpLeft(object sender, EventArgs e) {

    }
    private void CanEatDownRight(object sender, EventArgs e) {

    }
    private void CanEatUpRight(object sender, EventArgs e) {

    }

    private void CanEat(int y, int x) {
        for(int i=0;i<10;i++) {
            for(int j=0;j<10;j++) {
                this.Plateau[i,j].Enabled = false;
                this.Plateau[i,j].Click -= new EventHandler(GoHere);
                this.Plateau[i,j].Click -= new EventHandler(CanMoveLeft);
                this.Plateau[i,j].Click -= new EventHandler(CanMoveRight);
                this.Plateau[i,j].Click -= new EventHandler(CanEatLeft);
                this.Plateau[i,j].Click -= new EventHandler(CanEatRight);
                this.Plateau[i,j].Click -= new EventHandler(GoAndEatLeft);
                this.Plateau[i,j].Click -= new EventHandler(GoAndEatRight);
            }
        }

        Button next;

        switch(this.player) {
            case 0:
                    if(y<8 && x<8) {
                        if((next=(Button)(this.Plateau[y+1,x+1].GetChildAtPoint(new Point(0,0)))) != null && this.Plateau[y+2,x+2].GetChildAtPoint(new Point(0,0)) == null && next.BackgroundImage == this.PionBlanc) {
                            CanEatRight(this.Plateau[y,x],null);
                            this.again=true;
                        }
                    }
                    if(y<8 && x>1) {
                        if((next=(Button)(this.Plateau[y+1,x-1].GetChildAtPoint(new Point(0,0)))) != null && this.Plateau[y+2,x-2].GetChildAtPoint(new Point(0,0)) == null && next.BackgroundImage == this.PionBlanc) {
                            CanEatLeft(this.Plateau[y,x],null);
                            this.again=true;
                        }
                    }
            break;

            case 1:
                    if(y>1 && x<8) {
                        if((next=(Button)(this.Plateau[y-1,x+1].GetChildAtPoint(new Point(0,0)))) != null && this.Plateau[y-2,x+2].GetChildAtPoint(new Point(0,0)) == null && next.BackgroundImage == this.PionNoir) {
                            CanEatRight(this.Plateau[y,x],null);
                            this.again=true;
                        }
                    }
                    if(y>1 && x>1) {
                        if((next=(Button)(this.Plateau[y-1,x-1].GetChildAtPoint(new Point(0,0)))) != null && this.Plateau[y-2,x-2].GetChildAtPoint(new Point(0,0)) == null && next.BackgroundImage == this.PionNoir) {
                            CanEatLeft(this.Plateau[y,x],null);
                            this.again=true;
                            
                        }
                    }
            break;
        }
    }
    private void Verify(int y, int x) {
        Button next;

        switch(this.player) {
            case 0:
                    if(y<9) {
                        if(x<9) {
                            if((next=(Button)(this.Plateau[y+1,x+1].GetChildAtPoint(new Point(0,0)))) == null) {
                                this.Plateau[y,x].Enabled = true;
                                this.Plateau[y,x].Click += new EventHandler(CanMoveRight);
                            }
                            else if(y<8 && x<8) {
                                if(this.Plateau[y+2,x+2].GetChildAtPoint(new Point(0,0)) == null && next.BackgroundImage == this.PionBlanc) {
                                    this.Plateau[y,x].Enabled = true;
                                    this.Plateau[y,x].Click += new EventHandler(CanEatRight);
                                }
                            }
                        }
                        if(x>0) {
                            if((next=(Button)(this.Plateau[y+1,x-1].GetChildAtPoint(new Point(0,0)))) == null) {
                                this.Plateau[y,x].Enabled = true;
                                this.Plateau[y,x].Click += new EventHandler(CanMoveLeft);
                            }
                            else if(y<8 && x>1) {
                                if(this.Plateau[y+2,x-2].GetChildAtPoint(new Point(0,0)) == null && next.BackgroundImage == this.PionBlanc) {
                                    this.Plateau[y,x].Enabled = true;
                                    this.Plateau[y,x].Click += new EventHandler(CanEatLeft);
                                }
                            }

                        }
                    }
            break;

            case 1:
                    if(y>0) {
                        if(x<9) {
                            if((next=(Button)(this.Plateau[y-1,x+1].GetChildAtPoint(new Point(0,0)))) == null) {
                                this.Plateau[y,x].Enabled = true;
                                this.Plateau[y,x].Click += new EventHandler(CanMoveRight);
                            }
                            else if(y>1 && x<8) {
                                if(this.Plateau[y-2,x+2].GetChildAtPoint(new Point(0,0)) == null && next.BackgroundImage == this.PionNoir) {
                                    this.Plateau[y,x].Enabled = true;
                                    this.Plateau[y,x].Click += new EventHandler(CanEatRight);
                                }
                            }
                        }
                        if(x>0) {
                            if((next=(Button)(this.Plateau[y-1,x-1].GetChildAtPoint(new Point(0,0)))) == null) {
                                this.Plateau[y,x].Enabled = true;
                                this.Plateau[y,x].Click += new EventHandler(CanMoveLeft);
                            }
                            else if(y>1 && x>1) {
                                if(this.Plateau[y-2,x-2].GetChildAtPoint(new Point(0,0)) == null && next.BackgroundImage == this.PionNoir) {
                                    this.Plateau[y,x].Enabled = true;
                                    this.Plateau[y,x].Click += new EventHandler(CanEatLeft);
                                }
                            }
                        }
                    }
            break;
        }
    }
    private void VerifyDame(int y, int x) {
        Button next;
        if(y<9) {
            if(x<9) {
                if((next=(Button)(this.Plateau[y+1,x+1].GetChildAtPoint(new Point(0,0)))) == null) {
                    this.Plateau[y,x].Enabled = true;
                    this.Plateau[y,x].Click += new EventHandler(CanMoveDownRight);
                }
                else if(y<8 && x<8) {
                    if(this.Plateau[y+2,x+2].GetChildAtPoint(new Point(0,0)) == null && next.BackgroundImage == this.Plateau[y,x].BackgroundImage) {
                        this.Plateau[y,x].Enabled = true;
                        this.Plateau[y,x].Click += new EventHandler(CanEatDownRight);
                    }
                }
            }
            if(x>0) {
                if((next=(Button)(this.Plateau[y+1,x-1].GetChildAtPoint(new Point(0,0)))) == null) {
                    this.Plateau[y,x].Enabled = true;
                    this.Plateau[y,x].Click += new EventHandler(CanMoveDownLeft);
                }
                else if(y<8 && x>1) {
                    if(this.Plateau[y+2,x-2].GetChildAtPoint(new Point(0,0)) == null && next.BackgroundImage == this.Plateau[y,x].BackgroundImage) {
                        this.Plateau[y,x].Enabled = true;
                        this.Plateau[y,x].Click += new EventHandler(CanEatDownLeft);
                    }
                }
            }
        }
        if(y>0) {
            if(x<9) {
                if((next=(Button)(this.Plateau[y-1,x+1].GetChildAtPoint(new Point(0,0)))) == null) {
                    this.Plateau[y,x].Enabled = true;
                    this.Plateau[y,x].Click += new EventHandler(CanMoveUpRight);
                }
                else if(y>1 && x<8) {
                    if(this.Plateau[y+2,x+2].GetChildAtPoint(new Point(0,0)) == null && next.BackgroundImage == this.Plateau[y,x].BackgroundImage) {
                        this.Plateau[y,x].Enabled = true;
                        this.Plateau[y,x].Click += new EventHandler(CanEatUpRight);
                    }
                }
            }
            if(x>0) {
                if((next=(Button)(this.Plateau[y-1,x-1].GetChildAtPoint(new Point(0,0)))) == null) {
                    this.Plateau[y,x].Enabled = true;
                    this.Plateau[y,x].Click += new EventHandler(CanMoveUpLeft);
                }
                else if(y>1 && x>1) {
                    if(this.Plateau[y+2,x-2].GetChildAtPoint(new Point(0,0)) == null && next.BackgroundImage == this.Plateau[y,x].BackgroundImage) {
                        this.Plateau[y,x].Enabled = true;
                        this.Plateau[y,x].Click += new EventHandler(CanEatUpLeft);
                    }
                }
            }
        }
    }
    private void NextPlayer() {
        for(int i=0;i<10;i++) {
            for(int j=0;j<10;j++) {
                this.Plateau[i,j].Enabled = false;
                this.Plateau[i,j].Click -= new EventHandler(GoHere);
                this.Plateau[i,j].Click -= new EventHandler(CanMoveLeft);
                this.Plateau[i,j].Click -= new EventHandler(CanMoveRight);
                this.Plateau[i,j].Click -= new EventHandler(CanEatLeft);
                this.Plateau[i,j].Click -= new EventHandler(CanEatRight);
                this.Plateau[i,j].Click -= new EventHandler(GoAndEatLeft);
                this.Plateau[i,j].Click -= new EventHandler(GoAndEatRight);
                this.Plateau[i,j].Click -= new EventHandler(ClickPion);
            }
            
        }
        if(this.player == 0) {
            this.player = 1;
        }
        else {
            this.player = 0;
        }
        for(int i=0;i<20;i++) {
            if(this.Dames[this.player,i].Parent != null) {
                this.Dames[this.player,i].Parent.Click += new EventHandler(ClickPion);
            }
            if(this.Pions[this.player,i].Parent != null) {
                this.Pions[this.player,i].Parent.Click += new EventHandler(ClickPion);
            }
            
            
            for(int y=0;y<10;y++) {
                for(int x=0;x<10;x++) {
                    if(this.Pions[this.player,i].Parent == this.Plateau[y,x]) {
                        Verify(y,x);
                    }
                    else if(this.Dames[this.player,i].Parent == this.Plateau[y,x]) {
                        VerifyDame(y,x);
                    }
                }
            }
        } 
    }


    public void Resize_Dame() {
        Size CaseSize = new Size(this.window.GameWindow.Width/20,this.window.GameWindow.Width/20);

        this.GameP.Size = this.window.GameWindow.Size;



        for(int i=0;i<10;i++) {
            for(int j=0;j<10;j++) {
                this.Plateau[i,j].Size = CaseSize;
            }
            
        }

        
        this.Plateau[0,0].Location = new Point(this.GameP.Width/2-this.Plateau[0,0].Width*5, this.GameP.Height/2-this.Plateau[0,0].Height*5);
        for(int i=0;i<10;i++) {
            if(i!=0) {
                this.Plateau[i,0].Location = new Point(this.Plateau[i-1,0].Left, this.Plateau[i-1,0].Bottom);
            }
            for(int j=1;j<10;j++) {
                this.Plateau[i,j].Location = new Point(this.Plateau[i,j-1].Right, this.Plateau[i,j-1].Top);
            }
        }

        for(int i=0;i<20;i++) {
            for(int j=0;j<2;j++) {
                this.Pions[j,i].Size = CaseSize;
                this.Dames[j,i].Size = CaseSize;
                this.Pions[j,i].Size = CaseSize;
            }
        }

        this.jouer.Size = new Size(Plateau[0,0].Width*2,Plateau[0,0].Height);
        this.jouer.Location = new Point(this.Plateau[0,9].Right+this.jouer.Width, this.Plateau[9,9].Top);
        this.jouer.Font = new Font(this.jouer.Font.Name, this.jouer.Width/10);
    }







    public bool IsHere() {
        if(this.window.GameWindow.Controls.Contains(this.GameP)) {
            return true;
        }
        else {
            return false;
        }
    }
    public void add() {
        this.window.GameWindow.Controls.Add(this.GameP);
    }
}