using System;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;

public class Chess {

    private API window;
    private Panel GameP;
    private Button[,] Plateau;
    private Button[,] Pions;
    private Button jouer;
    private Button more;
    private Panel More;
    private Button setting;
    private Button exit;
    private int player=0;
    private Button saver;




    public Chess(API window) {

        this.window = window;

        this.GameP = new Panel();
        this.jouer = new Button();
        this.more = new Button();
        this.Plateau = new Button[8,8];
        this.Pions = new Button[2,16];

        Init();


    }




    private void Init() {

        this.GameP.Controls.Add(this.jouer);


        Size CaseSize = new Size(this.window.GameWindow.Width/15,this.window.GameWindow.Width/15);

        this.GameP.Size = this.window.GameWindow.Size;
        this.GameP.BackColor = Color.LightGray;



        for(int i=0;i<8;i++) {
            for(int j=0;j<8;j++) {
                this.Plateau[i,j] = new Button();
                this.GameP.Controls.Add(this.Plateau[i,j]);
                this.Plateau[i,j].Size = CaseSize;
                this.Plateau[i,j].Enabled = false;
                this.Plateau[i,j].FlatStyle = FlatStyle.Flat;
                this.Plateau[i,j].FlatAppearance.BorderSize = 0;
                if(i%2 == 0) {
                    if(j%2 == 0) {
                        this.Plateau[i,j].BackColor = Color.White;
                    }
                    else {
                        this.Plateau[i,j].BackColor = Color.Black;
                    }    
                }
                else {
                    if(j%2 == 0) {
                        this.Plateau[i,j].BackColor = Color.Black;
                    }
                    else {
                        this.Plateau[i,j].BackColor = Color.White;
                    }
                }
            }
        }

        
        this.Plateau[0,0].Location = new Point(this.GameP.Width/2-this.Plateau[0,0].Width*4, this.GameP.Height/2-this.Plateau[0,0].Height*4);
        for(int i=0;i<8;i++) {
            if(i != 0) {
                this.Plateau[i,0].Location = new Point(this.Plateau[i-1,0].Left, this.Plateau[i-1,0].Bottom);
            }
            for(int j=1;j<8;j++) {
                this.Plateau[i,j].Location = new Point(this.Plateau[i,j-1].Right, this.Plateau[i,j-1].Top);
            }
        }

        for(int i=0;i<2;i++) {
            for(int j=0;j<16;j++) {
                this.Pions[i,j] = new Button();
                this.Pions[i,j].FlatStyle = FlatStyle.Flat;
                this.Pions[i,j].FlatAppearance.BorderSize = 0;
                this.Pions[i,j].BackgroundImageLayout = ImageLayout.Zoom;
                this.Pions[i,j].Size = CaseSize;
                switch(j) {
                    case 0:
                    case 7:
                            if(i==0) {
                                this.Pions[i,j].BackgroundImage = Image.FromFile("img/Chess/tourBlanc.png");
                                this.Plateau[7,j].Controls.Add(this.Pions[i,j]);
                            }
                            else {
                                this.Pions[i,j].BackgroundImage = Image.FromFile("img/Chess/tourNoir.png");
                                this.Plateau[0,j].Controls.Add(this.Pions[i,j]);
                            }
                    break;

                    case 1:
                    case 6:
                            if(i==0) {
                                this.Pions[i,j].BackgroundImage = Image.FromFile("img/Chess/ChevBlanc.png");
                                this.Plateau[7,j].Controls.Add(this.Pions[i,j]);
                            }
                            else {
                                this.Pions[i,j].BackgroundImage = Image.FromFile("img/Chess/ChevNoir.png");
                                this.Plateau[0,j].Controls.Add(this.Pions[i,j]);
                            }
                    break;

                    case 2:
                    case 5:
                            if(i==0) {
                                this.Pions[i,j].BackgroundImage = Image.FromFile("img/Chess/FouBlanc.png");
                                this.Plateau[7,j].Controls.Add(this.Pions[i,j]);
                            }
                            else {
                                this.Pions[i,j].BackgroundImage = Image.FromFile("img/Chess/FouNoir.png");
                                this.Plateau[0,j].Controls.Add(this.Pions[i,j]);
                            }
                    break;

                    case 3:
                            if(i==0) {
                                this.Pions[i,j].BackgroundImage = Image.FromFile("img/Chess/DameBlanc.png");
                                this.Plateau[7,j].Controls.Add(this.Pions[i,j]);
                            }
                            else {
                                this.Pions[i,j].BackgroundImage = Image.FromFile("img/Chess/DameNoir.png");
                                this.Plateau[0,j].Controls.Add(this.Pions[i,j]);
                            }
                    break;

                    case 4:
                            if(i==0) {
                                this.Pions[i,j].BackgroundImage = Image.FromFile("img/Chess/RoiBlanc.png");
                                this.Plateau[7,j].Controls.Add(this.Pions[i,j]);
                            }
                            else {
                                this.Pions[i,j].BackgroundImage = Image.FromFile("img/Chess/RoiNoir.png");
                                this.Plateau[0,j].Controls.Add(this.Pions[i,j]);
                            }
                    break;

                    case 8:
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                    case 15:
                            if(i==0) {
                                this.Pions[i,j].BackgroundImage = Image.FromFile("img/Chess/PionBlanc.png");
                                this.Plateau[6,j-8].Controls.Add(this.Pions[i,j]);
                            }
                            else {
                                this.Pions[i,j].BackgroundImage = Image.FromFile("img/Chess/PionNoir.png");
                                this.Plateau[1,j-8].Controls.Add(this.Pions[i,j]);
                            } 
                    break;
                }
            }
        }





        this.jouer.Size = new Size(Plateau[0,0].Width*2,Plateau[0,0].Height);
        this.jouer.Location = new Point(this.Plateau[7,7].Right+this.jouer.Width/2, this.Plateau[7,7].Top);
        this.jouer.Text = "PLAY";
        this.jouer.BackColor = Color.Gray;
        this.jouer.FlatStyle = FlatStyle.Flat;
        this.jouer.FlatAppearance.BorderSize = this.jouer.Width/10;
        this.jouer.FlatAppearance.BorderColor = Color.LightGray;
        this.jouer.Font = new Font(this.jouer.Font.Name, this.jouer.Width/10);
        this.jouer.Click += new EventHandler(jouer_Click);





        Init_More();
    }
    private void Init_More() {
        this.exit = new Button();
        this.setting = new Button();
        this.More = new Panel();

        this.More.Controls.Add(this.exit);
        this.More.Controls.Add(this.setting);

        this.More.Size = new Size(this.more.Width*4, this.more.Height*2);
        this.More.Location = new Point(this.more.Right-this.More.Width,(int)(this.more.Bottom*1.1));
        this.More.BackColor = this.window.ColorParamButton;
        
        
        this.setting.Text = "Paramètres";
        this.setting.Size = new Size(this.More.Width,this.More.Height/2);
        this.setting.FlatStyle = FlatStyle.Flat;
        this.setting.FlatAppearance.BorderSize = 0;
        this.setting.FlatAppearance.MouseOverBackColor = this.exit.FlatAppearance.CheckedBackColor;
        this.setting.BackColor = this.window.ColorParam;
        this.setting.Click += new EventHandler(setting_Click);


        this.exit.Text = "Retourner Au Menu";
        this.exit.Size = new Size(this.More.Width,this.More.Height/2);
        this.exit.Location = new Point(this.setting.Left,this.setting.Bottom);
        this.exit.FlatStyle = FlatStyle.Flat;
        this.exit.FlatAppearance.BorderSize = 0;
        this.exit.FlatAppearance.MouseOverBackColor = this.exit.FlatAppearance.CheckedBackColor;
        this.exit.BackColor = this.window.ColorParam;
        this.exit.Click += new EventHandler(Menu_Return);
    }

    private void Menu_Return(object sender, EventArgs e) {
        this.GameP.Controls.Remove(this.More);
        this.more.Enabled = true;
        this.window.GameWindow.Controls.Remove(this.GameP);
        this.window.Home();
    }
    private void setting_Click(object sender, EventArgs e) {
        this.window.settingWindow.add();
        this.window.settingWindow.Resize_Setting();
    }
    private void more_Click(object sender, EventArgs e) {
        this.More.Location = new Point(this.more.Right-this.More.Width,this.more.Bottom+this.more.Height/20);
        this.GameP.Controls.Add(this.More);
        this.more.Enabled = false;
    }
    private void jouer_Click(object sender, EventArgs e) {
        for(int i=0;i<16;i++) {
            for(int j=0;j<8;j++) {
                for(int n=0;n<8;n++) {
                    if(this.Pions[this.player,i].Parent == this.Plateau[j,n]) {
                        Verify(i, n, j);
                    }
                }
            }
            
        }
        this.GameP.Controls.Remove(this.jouer);
        this.jouer.Click -= new EventHandler(jouer_Click);
    }

    public void add() {
        this.window.GameWindow.Controls.Add(this.GameP);
    }
    public void Resize_Chess() {
        Size CaseSize = new Size(this.window.GameWindow.Width/15,this.window.GameWindow.Width/15);

        this.GameP.Size = this.window.GameWindow.Size;

        for(int i=0;i<2;i++) {
            for(int j=0;j<16;j++) {
                this.Pions[i,j].Size = CaseSize;
            }
        }

        
        
        for(int i=0;i<8;i++) {
            for(int j=0;j<8;j++) {
                this.Plateau[i,j].Size = CaseSize;
            }
        }

        
        this.Plateau[0,0].Location = new Point(this.GameP.Width/2-this.Plateau[0,0].Width*4, this.GameP.Height/2-this.Plateau[0,0].Height*4);
        for(int i=0;i<8;i++) {
            if(i != 0) {
                this.Plateau[i,0].Location = new Point(this.Plateau[i-1,0].Left, this.Plateau[i-1,0].Bottom);
            }
            for(int j=1;j<8;j++) {
                this.Plateau[i,j].Location = new Point(this.Plateau[i,j-1].Right, this.Plateau[i,j-1].Top);
            }
        }

        this.jouer.Size = new Size(Plateau[0,0].Width*2,Plateau[0,0].Height);
        this.jouer.Location = new Point(this.Plateau[7,7].Right+this.jouer.Width/2, this.Plateau[7,7].Top);
        this.jouer.FlatAppearance.BorderSize = this.jouer.Width/10;
        this.jouer.Font = new Font(this.jouer.Font.Name, this.jouer.Width/10);




        this.More.Size = new Size(this.more.Width*4, this.more.Height*2);
        this.More.Location = new Point(this.more.Right-this.More.Width,(int)(this.more.Bottom*1.1));

        this.setting.Size = new Size(this.More.Width,this.More.Height/2);

        this.exit.Size = new Size(this.More.Width,this.More.Height/2);
        this.exit.Location = new Point(this.setting.Left,this.setting.Bottom);
    }




    private void NextPlayer() {
        if(this.player == 0) {
            this.player = 1;
        }
        else {
            this.player = 0;
        }
        for (int i=0;i<16;i++) {
            for(int j=0;j<8;j++) {
                for(int n=0;n<8;n++) {
                    if(this.Pions[this.player,i].Parent == this.Plateau[j,n]) {
                        Verify(i, n, j);
                    }
                }
            }
        }
    }
    private void Verify(int pion, int x, int y) {
        Button Case = this.Plateau[y,x];
        switch (pion) {
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                    if(this.player == 0) {
                        if(this.Plateau[y-1,x].GetChildAtPoint(new Point(0,0)) == null) {
                            Case.Enabled = true;
                            Case.Click += new EventHandler(PionCanAvance);
                        }
                        if(x>0) {
                            if(this.Plateau[y-1,x-1].GetChildAtPoint(new Point(0,0)) != null) {
                                Case.Enabled = true;
                                Case.Click += new EventHandler(PionCanMangeLeft);
                            }
                        }
                        if(x<7) {
                            if(this.Plateau[y-1,x+1].GetChildAtPoint(new Point(0,0)) != null) {
                                this.Pions[this.player,pion].Parent.Enabled = true;
                                this.Pions[this.player,pion].Parent.Click += new EventHandler(PionCanMangeRight);
                            }
                        }
                    }
                    else {
                        if(this.Plateau[y+1,x].GetChildAtPoint(new Point(0,0)) == null) {
                            Case.Enabled = true;
                            Case.Click += new EventHandler(PionCanAvance);
                        }
                        if(x>0) {
                            if(this.Plateau[y+1,x-1].GetChildAtPoint(new Point(0,0)) != null) {
                                this.Pions[this.player,pion].Parent.Enabled = true;
                                this.Pions[this.player,pion].Parent.Click += new EventHandler(PionCanMangeRight);
                            }
                        }
                        if(x<7) {
                            if(this.Plateau[y+1,x+1].GetChildAtPoint(new Point(0,0)) != null) {
                                this.Pions[this.player,pion].Parent.Enabled = true;
                                this.Pions[this.player,pion].Parent.Click += new EventHandler(PionCanMangeLeft);
                            }
                        }
                    }
            break;

            case 1:
            case 6:
                    for(int i=0;i<8;i++) {
                        ChevMove(i, y, x);
                    }
            break;
        }
    }

    private void PionCanAvance(object sender, EventArgs e) {
        int x=-1,y=-1;
        Button Case = (Button)sender;

        if(this.saver != null) {
            this.saver.Enabled = true;
        }

        this.saver = Case;



        Case.Enabled = false;

        for(int i=0;i<8;i++) {
            for(int j=0;j<8;j++) {
                if(Case == this.Plateau[i,j]) {
                    x=j;
                    y=i;
                }
            }
        }
        if(this.player == 0) {
            this.Plateau[y-1,x].Enabled = true;
            this.Plateau[y-1,x].Click += new EventHandler(GoHere);
        }
        else {
            this.Plateau[y+1,x].Enabled = true;
            this.Plateau[y+1,x].Click += new EventHandler(GoHere);
        }
    }
    private void PionCanMangeLeft(object sender, EventArgs e) {

    }
    private void PionCanMangeRight(object sender, EventArgs e) {

    }


    private void ChevMove0(object sender, EventArgs e) {
        int x=-1, y=-1;
        Button Case = (Button)sender;

        if(this.saver != null) {
            this.saver.Enabled = true;
        }

        this.saver = Case;

        for(int i=0;i<8;i++) {
            for(int j=0;j<8;j++) {
                if(this.Plateau[i,j] == Case) {
                    x=j;
                    y=i;
                }
            }
        }
        this.Plateau[y-1,x-2].Enabled = true;
        this.Plateau[y-1,x-2].Click += new EventHandler(GoHere);
    }
    private void ChevMove1(object sender, EventArgs e) {
        int x=-1, y=-1;
        Button Case = (Button)sender;

        if(this.saver != null) {
            this.saver.Enabled = true;
        }

        this.saver = Case;

        for(int i=0;i<8;i++) {
            for(int j=0;j<8;j++) {
                if(this.Plateau[i,j] == Case) {
                    x=j;
                    y=i;
                }
            }
        }
        this.Plateau[y-2,x-1].Enabled = true;
        this.Plateau[y-2,x-1].Click += new EventHandler(GoHere);
    }
    private void ChevMove2(object sender, EventArgs e) {
        int x=-1, y=-1;
        Button Case = (Button)sender;

        if(this.saver != null) {
            this.saver.Enabled = true;
        }

        this.saver = Case;

        for(int i=0;i<8;i++) {
            for(int j=0;j<8;j++) {
                if(this.Plateau[i,j] == Case) {
                    x=j;
                    y=i;
                }
            }
        }
        this.Plateau[y-2,x+1].Enabled = true;
        this.Plateau[y-2,x+1].Click += new EventHandler(GoHere);
    }
    private void ChevMove3(object sender, EventArgs e) {
        int x=-1, y=-1;
        Button Case = (Button)sender;

        if(this.saver != null) {
            this.saver.Enabled = true;
        }

        this.saver = Case;

        for(int i=0;i<8;i++) {
            for(int j=0;j<8;j++) {
                if(this.Plateau[i,j] == Case) {
                    x=j;
                    y=i;
                }
            }
        }
        this.Plateau[y-1,x+2].Enabled = true;
        this.Plateau[y-1,x+2].Click += new EventHandler(GoHere);
    }
    private void ChevMove4(object sender, EventArgs e) {
        int x=-1, y=-1;
        Button Case = (Button)sender;

        if(this.saver != null) {
            this.saver.Enabled = true;
        }

        this.saver = Case;

        for(int i=0;i<8;i++) {
            for(int j=0;j<8;j++) {
                if(this.Plateau[i,j] == Case) {
                    x=j;
                    y=i;
                }
            }
        }
        this.Plateau[y+2,x+1].Enabled = true;
        this.Plateau[y+2,x+1].Click += new EventHandler(GoHere);
    }
    private void ChevMove5(object sender, EventArgs e) {
        int x=-1, y=-1;
        Button Case = (Button)sender;

        if(this.saver != null) {
            this.saver.Enabled = true;
        }

        this.saver = Case;

        for(int i=0;i<8;i++) {
            for(int j=0;j<8;j++) {
                if(this.Plateau[i,j] == Case) {
                    x=j;
                    y=i;
                }
            }
        }
        this.Plateau[y+1,x+2].Enabled = true;
        this.Plateau[y+1,x+2].Click += new EventHandler(GoHere);
    }
    private void ChevMove6(object sender, EventArgs e) {
        int x=-1, y=-1;
        Button Case = (Button)sender;

        if(this.saver != null) {
            this.saver.Enabled = true;
        }

        this.saver = Case;

        for(int i=0;i<8;i++) {
            for(int j=0;j<8;j++) {
                if(this.Plateau[i,j] == Case) {
                    x=j;
                    y=i;
                }
            }
        }
        this.Plateau[y-2,x+1].Enabled = true;
        this.Plateau[y-2,x+1].Click += new EventHandler(GoHere);
    }
    private void ChevMove7(object sender, EventArgs e) {
        int x=-1, y=-1;
        Button Case = (Button)sender;

        if(this.saver != null) {
            this.saver.Enabled = true;
        }

        this.saver = Case;

        for(int i=0;i<8;i++) {
            for(int j=0;j<8;j++) {
                if(this.Plateau[i,j] == Case) {
                    x=j;
                    y=i;
                }
            }
        }
        this.Plateau[y-1,x+2].Enabled = true;
        this.Plateau[y-1,x+2].Click += new EventHandler(GoHere);
    }
    private void GoHere(object sender, EventArgs e) {
        Button sup;
        Button Case = (Button)sender;
        Button pion = (Button)(this.saver.GetChildAtPoint(new Point(0,0)));
        this.saver.Controls.Remove(pion);
        if((sup=(Button)(Case.GetChildAtPoint(new Point(0,0)))) != null) {
            Case.Controls.Remove(sup);
        }
        Case.Controls.Add(pion);
    }


    private void ChevMove(int move, int y, int x) {
        switch(move) {
            case 0: if(x>1 && y>0) {
                        if(this.Plateau[y-1,x-2].GetChildAtPoint(new Point(0,0)) == null) {
                            this.Plateau[y,x].Enabled = true;
                            this.Plateau[y,x].Click += new EventHandler(ChevMove0);
                        }
                    }
            break;

            case 1: if(x>0 && y>1) {
                        if(this.Plateau[y-2,x-1].GetChildAtPoint(new Point(0,0)) == null) {
                            this.Plateau[y,x].Enabled = true;
                            this.Plateau[y,x].Click += new EventHandler(ChevMove1);
                        }
                    }
            break;

            case 2: if(x<7 && y>1) {
                        if(this.Plateau[y-2,x+1].GetChildAtPoint(new Point(0,0)) == null) {
                            this.Plateau[y,x].Enabled = true;
                            this.Plateau[y,x].Click += new EventHandler(ChevMove2);
                        }
                    }
            break;

            case 3: if(x<6 && y>0) {
                        if(this.Plateau[y-1,x+2].GetChildAtPoint(new Point(0,0)) == null) {
                            this.Plateau[y,x].Enabled = true;
                            this.Plateau[y,x].Click += new EventHandler(ChevMove3);
                        }
                    }
            break;

            case 4: if(x<7 && y<6) {
                        if(this.Plateau[y+2,x+1].GetChildAtPoint(new Point(0,0)) == null) {
                            this.Plateau[y,x].Enabled = true;
                            this.Plateau[y,x].Click += new EventHandler(ChevMove4);
                        }
                    }
            break;

            case 5: if(x<6 && y<7) {
                        if(this.Plateau[y+1,x+2].GetChildAtPoint(new Point(0,0)) == null) {
                            this.Plateau[y,x].Enabled = true;
                            this.Plateau[y,x].Click += new EventHandler(ChevMove5);
                        }
                    }
            break;

            case 6: if(x<7 && y>1) {
                        if(this.Plateau[y-2,x+1].GetChildAtPoint(new Point(0,0)) == null) {
                            this.Plateau[y,x].Enabled = true;
                            this.Plateau[y,x].Click += new EventHandler(ChevMove6);
                        }
                    }
            break;

            case 7: if(x<6 && y>0) {
                        if(this.Plateau[y-1,x+2].GetChildAtPoint(new Point(0,0)) == null) {
                            this.Plateau[y,x].Enabled = true;
                            this.Plateau[y,x].Click += new EventHandler(ChevMove7);
                        }
                    }
            break;
        }
    }

}