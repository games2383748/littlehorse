using System;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;

public class Setting : Form{
    //Variables des paramètres

    private Size BuSi;
    private Panel SettingP;
    private Button affichage;
    private Button affichage1;
    private Button affichage2;
    private Button affichage3;
    private Button resolution;
    private Button applicate;
    private Button back;
    private API window;
    private FormBorderStyle WindowStyle;
    private Size WindowSize;
    private Point WindowLocate;

    //Contsante
    private Color ColorParam = Color.LightGray;
    private Color ColorParamButton = Color.Gray;

    public Setting(API window) {
        this.window = window;

        this.SettingP = new Panel();
        
        this.affichage = new Button();
        this.affichage1 = new Button();
        this.affichage2 = new Button();
        this.affichage3 = new Button();
        
        this.resolution = new Button();

        this.applicate = new Button();
        this.back = new Button();


        this.SettingP.Size = new Size(this.window.GameWindow.Width-this.window.GameWindow.Width/100,this.window.GameWindow.Height-this.window.GameWindow.Height/100);
        this.SettingP.Location = new Point(this.window.GameWindow.Width/500,this.window.GameWindow.Height/200);
        this.SettingP.BackColor = ColorParam;

        this.SettingP.Controls.Add(this.affichage);
        this.SettingP.Controls.Add(this.affichage1);
        this.SettingP.Controls.Add(this.affichage2);
        this.SettingP.Controls.Add(this.affichage3);

        this.SettingP.Controls.Add(this.resolution);
        
        this.SettingP.Controls.Add(this.applicate);
        this.SettingP.Controls.Add(this.back);

        this.BuSi = new Size(this.SettingP.Width/10,this.SettingP.Width/30);

        
        this.affichage.BackColor = ColorParamButton;
        this.affichage1.BackColor = Color.Blue;
        this.affichage2.BackColor = Color.Blue;
        this.affichage3.BackColor = Color.LightBlue;

        this.resolution.BackColor = ColorParamButton;

        this.applicate.BackColor = ColorParamButton;
        this.back.BackColor = ColorParamButton;


        this.affichage.Text = "Affichage";
        this.affichage1.Text = "Fenetré";
        this.affichage2.Text = "Fenetré sans bordure";
        this.affichage3.Text = "Pleine écran";

        this.resolution.Text = "Résolution";

        this.applicate.Text = "Appliquer";

        this.affichage.TextAlign = ContentAlignment.MiddleLeft;
        this.resolution.TextAlign = ContentAlignment.MiddleLeft;

        this.affichage1.Size = this.BuSi;
        this.affichage2.Size = this.BuSi;
        this.affichage3.Size = this.BuSi;

        this.affichage3.Location = new Point(this.SettingP.Width-this.affichage3.Width*2,this.SettingP.Width/20);
        this.affichage2.Location = new Point(this.affichage3.Left-this.affichage2.Width,this.affichage3.Top);
        this.affichage1.Location = new Point(this.affichage2.Left-this.affichage1.Width,this.affichage3.Top);
        

        this.affichage.Size = new Size(this.affichage1.Left - this.affichage1.Width/2, this.affichage3.Height);
        
        this.resolution.Size = new Size(this.affichage.Width, this.affichage.Height);
        

        this.affichage.Location = new Point(this.affichage1.Width/2,this.affichage3.Top);
        
        this.resolution.Location = new Point(this.affichage.Left,this.affichage.Bottom+this.resolution.Height/2);

        this.applicate.Size = BuSi;
        this.applicate.Location = new Point(this.SettingP.Width-this.applicate.Width*2,this.SettingP.Height-this.applicate.Height*3);
        this.back.Size = new Size(this.affichage1.Height,this.affichage1.Height);
        this.back.Location = new Point(this.back.Width/2,this.back.Height/2);


        this.affichage.FlatStyle = FlatStyle.Flat;
        this.affichage.FlatAppearance.BorderSize = 2;
        this.affichage.Enabled = false;
        this.affichage1.FlatStyle = FlatStyle.Flat;
        this.affichage1.FlatAppearance.BorderSize = 2;
        this.affichage2.FlatStyle = FlatStyle.Flat;
        this.affichage2.FlatAppearance.BorderSize = 2;
        this.affichage3.FlatStyle = FlatStyle.Flat;
        this.affichage3.FlatAppearance.BorderSize = 2;

        this.resolution.FlatStyle = FlatStyle.Flat;
        this.resolution.FlatAppearance.BorderSize = 2;
        this.resolution.Enabled = false;

        this.applicate.FlatStyle = FlatStyle.Flat;
        this.applicate.FlatAppearance.BorderSize = 2;
        this.back.FlatStyle = FlatStyle.Flat;
        this.back.FlatAppearance.BorderSize = 2;

        this.back.BackgroundImage = Image.FromFile("img/back.png");
        this.back.BackgroundImageLayout = ImageLayout.Zoom;
        

        this.affichage1.Click += new EventHandler(fenetre_Click);
        this.affichage2.Click += new EventHandler(fsb_Click);
        this.affichage3.Click += new EventHandler(pleinE_Click);

        this.applicate.Click += new EventHandler(applicate_Click);
        this.back.Click += new EventHandler(back_Click);
    }



    private void fenetre_Click(object sender, EventArgs e) {
        this.WindowStyle = FormBorderStyle.Sizable;
        this.WindowSize = new Size(1920/2,1080/2);
        this.WindowLocate = new Point(1920/5,1080/5);

        this.affichage1.BackColor = Color.LightBlue;
        this.affichage2.BackColor = Color.Blue;
        this.affichage3.BackColor = Color.Blue;
    }
    private void fsb_Click(object sender, EventArgs e) {
        this.WindowStyle = FormBorderStyle.None;

        this.affichage1.BackColor = Color.Blue;
        this.affichage2.BackColor = Color.LightBlue;
        this.affichage3.BackColor = Color.Blue;
    } 
    private void pleinE_Click(object sender, EventArgs e) {
        this.WindowStyle = FormBorderStyle.None;
        this.WindowLocate = new Point(0,0);
        this.WindowSize = new Size(1920,1080);

        this.affichage1.BackColor = Color.Blue;
        this.affichage2.BackColor = Color.Blue;
        this.affichage3.BackColor = Color.LightBlue;
    }
    private void applicate_Click(object sender, EventArgs e) {
        this.window.GameWindow.FormBorderStyle = this.WindowStyle;
        this.window.GameWindow.Size = this.WindowSize;
        this.window.GameWindow.Location = this.WindowLocate;
    }

    private void back_Click(object sender, EventArgs e) {
        this.window.GameWindow.Controls.Remove(this.SettingP);
    }


    public void Resize_Setting() {
        this.SettingP.Size = new Size(this.window.GameWindow.Width-this.window.GameWindow.Width/100,this.window.GameWindow.Height-this.window.GameWindow.Height/100);
        this.SettingP.Location = new Point(this.window.GameWindow.Width/500,this.window.GameWindow.Height/200);

        this.BuSi = new Size(this.SettingP.Width/10,this.SettingP.Width/30);

        this.affichage1.Size = this.BuSi;
        this.affichage2.Size = this.BuSi;
        this.affichage3.Size = this.BuSi;

        this.affichage3.Location = new Point(this.SettingP.Width-this.affichage3.Width*2,this.SettingP.Width/20);
        this.affichage2.Location = new Point(this.affichage3.Left-this.affichage2.Width,this.affichage3.Top);
        this.affichage1.Location = new Point(this.affichage2.Left-this.affichage1.Width,this.affichage3.Top);
        

        this.affichage.Size = new Size(this.affichage1.Left - this.affichage1.Width/2, this.affichage3.Height);
        
        this.resolution.Size = new Size(this.affichage.Width, this.affichage.Height);
        

        this.affichage.Location = new Point(this.affichage1.Width/2,this.affichage3.Top);
        
        this.resolution.Location = new Point(this.affichage.Left,this.affichage.Bottom+this.resolution.Height/2);

        this.applicate.Size = BuSi;
        this.applicate.Location = new Point(this.SettingP.Width-this.applicate.Width*2,this.SettingP.Height-this.applicate.Height*3);
        this.back.Size = new Size(this.affichage1.Height,this.affichage1.Height);
        this.back.Location = new Point(this.back.Width/2,this.back.Height/2);

    }
    public bool IsHere() {
        if(this.window.GameWindow.Controls.Contains(this.SettingP)) {
            return true;
        }
        else {
            return false;
        }
    }
    public void add() {
        this.window.GameWindow.Controls.Add(this.SettingP);
        this.SettingP.BringToFront();
    }
}