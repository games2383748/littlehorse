using System;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;


public class API : Form{
    
    //Paneaux
    public Form GameWindow;
    public Setting settingWindow;
    public MenuP menuWindow;

    public Panel Launcher;

    //Contsante
    public Color ColorParam = Color.LightGray;
    public Color ColorParamButton = Color.Gray;
    

    public API(){
        this.GameWindow = new Form();
        this.GameWindow.FormBorderStyle = FormBorderStyle.None;
        this.GameWindow.TopMost = true;
        this.GameWindow.WindowState = FormWindowState.Maximized;

        this.Launcher = new Panel();

        this.GameWindow.SizeChanged += new EventHandler(Resize_Window);

        Bitmap ico = new Bitmap("img/logo.ico");
        IntPtr hico = ico.GetHicon();

        this.GameWindow.Icon = Icon.FromHandle(hico);

        this.GameWindow.BackColor = Color.Black;


        this.GameWindow.Text = "Little Horse";
        
        this.menuWindow = new MenuP(this);
        this.settingWindow = new Setting(this);
    }


    public void Home() {
        this.menuWindow.add();
    }
    private void Resize_Window(object sender, EventArgs e) {
        if(this.GameWindow.Controls.Contains(this.Launcher)) {
            this.Launcher.Size = this.GameWindow.Size;
        }
        if(this.menuWindow.IsHere()) {
            this.menuWindow.Resize_Menu();
        }
        if(this.settingWindow.IsHere()) {
            this.settingWindow.Resize_Setting();
        }
    }
    public void LunchGame() {
        this.Launcher.BackgroundImage = Image.FromFile("img/VirtuStudio.jpeg");
        this.Launcher.BackgroundImageLayout = ImageLayout.Zoom;
        this.Launcher.Size = this.GameWindow.Size;
        this.GameWindow.Controls.Add(this.Launcher);
        this.Launcher.Click += new EventHandler(Remove_Launcher);
        this.GameWindow.ShowDialog();
    }


    private void Remove_Launcher(object sender, EventArgs e) {
        this.menuWindow.add();
        this.GameWindow.Controls.Remove(this.Launcher);
        
    }
}