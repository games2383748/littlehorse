using System;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;

public class Morpion {
    private API window;
    private const int SizeGrille = 3;
    private Button[,] Grille;
    private bool player = true;
    private Panel GameP;
    private Button jouer;
    private Button more;
    private Button exit;


    public Morpion(API window) {
        this.window = window;

        this.GameP = new Panel();
        
        Init();
        Init_More();
    }
    

    private void Init() {
        this.GameP.Size = this.window.GameWindow.Size;
        this.Grille = new Button[SizeGrille,SizeGrille];
        this.jouer = new Button();
        this.more = new Button();
        
        this.GameP.Controls.Add(this.more);
        this.GameP.Controls.Add(this.jouer);
        
        this.GameP.BackColor = Color.LightGray;

        for(int i=0;i<SizeGrille;i++) {
            for(int j=0;j<SizeGrille;j++) {
                this.Grille[i,j] = new Button();
                this.GameP.Controls.Add(this.Grille[i,j]);

                this.Grille[i,j].Enabled = false;
                this.Grille[i,j].Size = new Size(this.GameP.Height/4,this.GameP.Height/4);
                this.Grille[i,j].FlatStyle = FlatStyle.Flat;
                this.Grille[i,j].FlatAppearance.BorderSize = 1;
                this.Grille[i,j].Font = new Font(this.Grille[i,j].Font.Name, this.Grille[i,j].Height - 4);
            }
        }

        this.Grille[0,0].Location = new Point(this.GameP.Width/2-(this.Grille[0,0].Width/2)*3, this.GameP.Height/2-(this.Grille[0,0].Width/2)*3);
        
        for(int i=0;i<SizeGrille;i++) {
            if(i != 0) {
                this.Grille[i,0].Location = new Point(this.Grille[i-1,0].Left, this.Grille[i-1,0].Bottom);
            }
            for(int j=1;j<SizeGrille;j++) {
                this.Grille[i,j].Location = new Point(this.Grille[i,j-1].Right, this.Grille[i,j-1].Top);
            }
        }

        this.jouer.Size = new Size(Grille[0,0].Width,Grille[0,0].Height/2);
        this.jouer.Location = new Point(this.Grille[2,2].Right+this.jouer.Width, this.Grille[2,2].Top);
        this.jouer.Text = "PLAY";
        this.jouer.BackColor = Color.Gray;
        this.jouer.FlatStyle = FlatStyle.Flat;
        this.jouer.FlatAppearance.BorderSize = this.jouer.Width/10;
        this.jouer.FlatAppearance.BorderColor = Color.LightGray;
        this.jouer.Font = new Font(this.jouer.Font.Name, this.jouer.Width/10);
        this.jouer.Click += new EventHandler(jouer_Click);

        this.more.Size = new Size(Grille[0,0].Width/2,Grille[0,0].Height/2);
        this.more.Location = new Point(this.GameP.Right-this.more.Width*2, this.GameP.Top+this.more.Height*2);
        this.more.FlatStyle = FlatStyle.Flat;
        this.more.FlatAppearance.BorderSize = 0;
        this.more.Click += new EventHandler(More_Click);
    }


    private void Init_More() {
        this.exit = new Button();
        this.setting = new Button();
        this.More = new Panel();

        this.More.Controls.Add(this.exit);
        this.More.Controls.Add(this.setting);

        this.More.Size = new Size(this.more.Width*4, this.more.Height*2);
        this.More.Location = new Point(this.more.Right-this.More.Width,(int)(this.more.Bottom*1.1));
        this.More.BackColor = ColorParamButton;
        
        
        this.setting.Text = "Paramètres";
        this.setting.Size = new Size(this.More.Width,this.More.Height/2);
        this.setting.FlatStyle = FlatStyle.Flat;
        this.setting.FlatAppearance.BorderSize = 0;
        this.setting.FlatAppearance.MouseOverBackColor = this.exit.FlatAppearance.CheckedBackColor;
        this.setting.BackColor = ColorParam;
        this.setting.Click += new EventHandler(setting_Click);


        this.exit.Text = "Retourner Au Menu";
        this.exit.Size = new Size(this.More.Width,this.More.Height/2);
        this.exit.Location = new Point(this.setting.Left,this.setting.Bottom);
        this.exit.FlatStyle = FlatStyle.Flat;
        this.exit.FlatAppearance.BorderSize = 0;
        this.exit.FlatAppearance.MouseOverBackColor = this.exit.FlatAppearance.CheckedBackColor;
        this.exit.BackColor = ColorParam;
        this.exit.Click += new EventHandler(Menu_Return);
    }


    private void Menu_Return(object sender, EventArgs e) {
        this.GameP.Controls.Remove(this.More);
        this.more.Enabled = true;
        this.window.GameWindow.Controls.Remove(this.GameP);
        this.window.Home();
    }
    private void setting_Click(object sender, EventArgs e) {
        this.window.settingWindow.add();
        this.window.settingWindow.Resize_Setting();
    }
    private void More_Click(object sender, EventArgs e) {
        this.More.Location = new Point(this.more.Right-this.More.Width,this.more.Bottom+this.more.Height/20);
        this.GameP.Controls.Add(this.More);
        this.more.Enabled = false;
    }

    private void jouer_Click(object sender, EventArgs e) {
        this.GameP.Controls.Remove(this.jouer);
        this.jouer.Click -= new EventHandler(jouer_Click);

        for(int i=0;i<SizeGrille;i++) {
            for(int j=0;j<SizeGrille;j++) {
                this.Grille[i,j].Enabled = true;
                this.Grille[i,j].Click += new EventHandler(Place_Mark);
            }
        }
    }
    private void Replay(object sender, EventArgs e) {
        this.GameP.Controls.Remove(this.jouer);
        for(int i=0;i<SizeGrille;i++) {
            for(int j=0;j<SizeGrille;j++) {
                this.Grille[i,j].Text = "";
                this.Grille[i,j].BackColor = Color.Transparent;
                this.Grille[i,j].Enabled = true;
                this.Grille[i,j].Click += new EventHandler(Place_Mark);
            }
        }
    }

    private void Place_Mark(object sender, EventArgs e) {
        Button Case = (Button) sender;

        if(this.player) {
            Case.Text = "X";
        }
        else {
            Case.Text = "O";
        }

        Case.Enabled = false;
        Case.Click -= new EventHandler(Place_Mark);

        if(TestPartieFini()) {
            for(int i=0;i<SizeGrille;i++) {
                for(int j=0;j<SizeGrille;j++) {
                    this.Grille[i,j].Enabled = false;
                    this.Grille[i,j].Click -= new EventHandler(Place_Mark);
                }
            }
            this.GameP.Controls.Add(this.jouer);
            this.jouer.Text = "Replay";
            this.jouer.Click += new EventHandler(Replay);
        }

        Next_Player();
    }



    private void Next_Player() {
        if(this.player) {
            this.player = false;
        }
        else {
            this.player = true;
        }
    }
    private bool TestPartieFini() {
        for(int i=0;i<SizeGrille;i++) {
            if(this.Grille[i,0].Text != "" && this.Grille[i,0].Text == this.Grille[i,1].Text && this.Grille[i,1].Text == this.Grille[i,2].Text) {
                this.Grille[i,0].BackColor = Color.Green;
                this.Grille[i,1].BackColor = Color.Green;
                this.Grille[i,2].BackColor = Color.Green;
                return true;
            }
        }
        for(int i=0;i<SizeGrille;i++) {
            if(this.Grille[0,i].Text != "" && this.Grille[0,i].Text == this.Grille[1,i].Text && this.Grille[1,i].Text == this.Grille[2,i].Text) {
                this.Grille[0,i].BackColor = Color.Green;
                this.Grille[1,i].BackColor = Color.Green;
                this.Grille[2,i].BackColor = Color.Green;
                return true;
            }
        }
        if(this.Grille[0,0].Text != "" && this.Grille[0,0].Text == this.Grille[1,1].Text && this.Grille[1,1].Text == this.Grille[2,2].Text) {
            this.Grille[0,0].BackColor = Color.Green;
            this.Grille[1,1].BackColor = Color.Green;
            this.Grille[2,2].BackColor = Color.Green;
            return true;
        }
        if(this.Grille[0,2].Text != "" && this.Grille[0,2].Text == this.Grille[1,1].Text && this.Grille[1,1].Text == this.Grille[2,0].Text) {
            this.Grille[0,2].BackColor = Color.Green;
            this.Grille[1,1].BackColor = Color.Green;
            this.Grille[2,0].BackColor = Color.Green;
            return true;
        }
        for(int i=0;i<SizeGrille;i++) {
            for(int j=0;j<SizeGrille;j++) {
                if(this.Grille[i,j].Text == "") {
                    return false;
                }
            }
        }


        return true;
    }



    public void Resize_Morpion() {
        this.GameP.Size = this.window.GameWindow.Size;


        for(int i=0;i<SizeGrille;i++) {
            for(int j=0;j<SizeGrille;j++) {
                this.Grille[i,j].Size = new Size(this.GameP.Height/4,this.GameP.Height/4);
            }
        }

        this.Grille[0,0].Location = new Point(this.GameP.Width/2-(this.Grille[0,0].Width/2)*3, this.GameP.Height/2-(this.Grille[0,0].Width/2)*3);
        
        for(int i=0;i<SizeGrille;i++) {
            if(i != 0) {
                this.Grille[i,0].Location = new Point(this.Grille[i-1,0].Left, this.Grille[i-1,0].Bottom);
            }
            for(int j=1;j<SizeGrille;j++) {
                this.Grille[i,j].Location = new Point(this.Grille[i,j-1].Right, this.Grille[i,j-1].Top);
            }
        }

        this.jouer.Size = new Size(Grille[0,0].Width,Grille[0,0].Height/2);
        this.jouer.Location = new Point(this.Grille[2,2].Right+this.jouer.Width, this.Grille[2,2].Top);
        this.jouer.FlatAppearance.BorderSize = this.jouer.Width/10;
        this.jouer.Font = new Font(this.jouer.Font.Name, this.jouer.Width/10);
    }

    public void add() {
        this.window.GameWindow.Controls.Add(this.GameP);
    }
}