using System;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;

public class MenuP : Form{
    //Jeux

    public GameColection Colection;
    private Panel MenuWindow;
    private Button play;
    private Button more;
    private Button exit;
    private Panel More;
    private Button setting;
    private API window;

    //Constante
    private Color ColorParam = Color.LightGray;
    private Color ColorParamButton = Color.Gray;

    public MenuP(API window) {

        this.window = window;
        this.MenuWindow = new Panel();
        this.play = new Button();
        this.more = new Button();
        this.Colection = new GameColection(window);

        this.MenuWindow.Size = this.window.GameWindow.Size;
        this.MenuWindow.BackgroundImage = Image.FromFile("img/MenuFont.jpg");
        this.MenuWindow.BackgroundImageLayout = ImageLayout.Zoom;

        this.MenuWindow.Click += new EventHandler(or_more_Click);

        this.MenuWindow.Controls.Add(play);
        this.MenuWindow.Controls.Add(more);
        
        this.more.BackgroundImage = Image.FromFile("img/more.png");
        this.more.BackColor = Color.Transparent;
        this.more.BackgroundImageLayout = ImageLayout.Zoom;
        this.more.Size = new Size(this.MenuWindow.Width/30,this.MenuWindow.Width/30);
        this.more.Location = new Point(this.MenuWindow.Width-this.more.Width*2,this.MenuWindow.Height/100);
        this.more.FlatStyle = FlatStyle.Flat;
        this.more.Click += new EventHandler(more_Click);
        this.more.FlatAppearance.BorderSize = 0;
        this.more.FlatAppearance.MouseOverBackColor = Color.Transparent;
        

        this.play.BackgroundImage = Image.FromFile("img/playbutton.png");
        this.play.BackgroundImageLayout = ImageLayout.Zoom;
        this.play.BackColor = Color.Transparent;
        this.play.Size = new Size(this.MenuWindow.Width/10,this.MenuWindow.Width/30);
        this.play.Location = new Point(this.MenuWindow.Width/2-this.play.Width/2,this.MenuWindow.Height/2-this.play.Height/2);
        this.play.FlatStyle = FlatStyle.Flat;
        this.play.Click += new EventHandler(play_Click);
        this.play.FlatAppearance.BorderSize = 0;
        this.play.FlatAppearance.MouseOverBackColor = this.play.FlatAppearance.CheckedBackColor;

        Init_More();
    }


    private void Init_More() {
        this.exit = new Button();
        this.setting = new Button();
        this.More = new Panel();

        this.More.Controls.Add(this.exit);
        this.More.Controls.Add(this.setting);

        this.More.Size = new Size(this.more.Width*4, this.more.Height*2);
        this.More.Location = new Point(this.more.Right-this.More.Width,(int)(this.more.Bottom*1.1));
        this.More.BackColor = ColorParamButton;
        
        
        this.setting.Text = "Paramètres";
        this.setting.Size = new Size(this.More.Width,this.More.Height/2);
        this.setting.FlatStyle = FlatStyle.Flat;
        this.setting.FlatAppearance.BorderSize = 0;
        this.setting.FlatAppearance.MouseOverBackColor = this.exit.FlatAppearance.CheckedBackColor;
        this.setting.BackColor = ColorParam;
        this.setting.Click += new EventHandler(setting_Click);


        this.exit.Text = "Quitter le jeu";
        this.exit.Size = new Size(this.More.Width,this.More.Height/2);
        this.exit.Location = new Point(this.setting.Left,this.setting.Bottom);
        this.exit.FlatStyle = FlatStyle.Flat;
        this.exit.FlatAppearance.BorderSize = 0;
        this.exit.FlatAppearance.MouseOverBackColor = this.exit.FlatAppearance.CheckedBackColor;
        this.exit.BackColor = ColorParam;
        this.exit.Click += new EventHandler(Quit_Game);
    }

    private void more_Click(object sender, EventArgs e) {
        this.More.Location = new Point(this.more.Right-this.More.Width,this.more.Bottom+this.more.Height/20);
        this.MenuWindow.Controls.Add(this.More);
        
        this.more.Enabled = false;
    }
    private void or_more_Click(object sender, EventArgs e) {
        if(this.MenuWindow.Controls.Contains(this.More)) {
            this.MenuWindow.Controls.Remove(this.More);
            this.more.Enabled = true;
        }
    }
    private void setting_Click(object sender, EventArgs e) {
        window.settingWindow.add();
        window.settingWindow.Resize_Setting();
    }
    private void play_Click(object sender, EventArgs e) {
        this.window.GameWindow.Controls.Remove(this.MenuWindow);
        this.Colection.add();
        this.Colection.Resize_Colection();
    }
    private void Quit_Game(object sender, EventArgs e) {
        this.window.GameWindow.Close();
    }

    public void add() {
        this.window.GameWindow.Controls.Add(this.MenuWindow);
        Resize_Menu();
    }
    public void Resize_Menu() {
        this.MenuWindow.Size = this.window.GameWindow.Size;
        
        this.more.Size = new Size(this.MenuWindow.Width/30,this.MenuWindow.Width/30);
        this.more.Location = new Point(this.MenuWindow.Width-this.more.Width*2,this.MenuWindow.Height/100);
        
        this.play.Size = new Size(this.MenuWindow.Width/10,this.MenuWindow.Width/30);
        this.play.Location = new Point(this.MenuWindow.Width/2-this.play.Width/2,this.MenuWindow.Height/2-this.play.Height/2);

        this.More.Size = new Size(this.more.Width*4, this.more.Height*2);
        this.More.Location = new Point(this.more.Right-this.More.Width,(int)(this.more.Bottom*1.1));
        
        this.setting.Size = new Size(this.More.Width,this.More.Height/2);

        this.exit.Size = new Size(this.More.Width,this.More.Height/2);
        this.exit.Location = new Point(this.setting.Left,this.setting.Bottom);
    }
    public bool IsHere() {
        if(this.window.GameWindow.Controls.Contains(this.MenuWindow)) {
            return true;
        }
        else {
            return false;
        }
    }
}