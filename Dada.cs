using System;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;

public class Dada : Form{

    //Variable du jeu
    private TextBox[] JoueurName;
    private Button Fake;
    private Color[] tampon;
    private Button[] CasePlay;
    private int nbwin=0;
    private Button[] winner;
    private Button turn;
    private int player=-1;
    private int avance;
    private Button[,] pions;
    private string[] joueurs;
    private Random de;
    private Size CaseSize;
    private Panel GameP;
    private Button[] home;
    private Button[] parcours;
    private Button[,] end;
    private Button jouer;
    private Button scrore;
    private API window;
    private Button more;
    private Color[] ColorWin;
    private Panel More;
    private Button exit;
    private Button setting;

    //Contsante
    private Color ColorParam = Color.LightGray;
    private Color ColorParamButton = Color.Gray;


    public Dada(API window) {
        this.window = window;
        Init_Partie();
    }
    

    private void Menu_Return(object sender, EventArgs e) {
        this.GameP.Controls.Remove(this.More);
        this.more.Enabled = true;
        this.window.GameWindow.Controls.Remove(this.GameP);
        this.window.Home();

    }
    private void Restart_Partie(object sender, EventArgs e) {
        Init_Partie();
    }
    private void home_Click(object sender, EventArgs e) {
        Button garde = (Button)sender;
        int n=-1;
        for(int i=0;i<4;i++) {
            if(this.home[i] == garde) {
                n=i;
            }
        }

        if(garde.Controls.Contains(this.JoueurName[n])) {
            garde.Controls.Remove(this.JoueurName[n]);
        }
        else {
            garde.Controls.Add(this.JoueurName[n]);
        }
    }
    private void jouer_Click(object sender, EventArgs e) {
        int nbj=0;
        this.turn = new Button();
        this.pions = new Button[4,2];
        this.joueurs = new string[4];
        this.scrore = new Button();

        this.GameP.Controls.Add(this.turn);
        this.GameP.Controls.Add(this.scrore);


        for(int i=0;i<4;i++) {
            if(this.home[i].Controls.Contains(this.JoueurName[i])) {
                this.joueurs[i] = "J"+(i+1);    nbj++;
                this.home[i].Controls.Remove(this.JoueurName[i]);
            }
            else {
                this.joueurs[i] = "X";
            }
        }
        if(nbj<2) {
            return;
        }
        
        do {
            this.player+=1;
            this.player%=4;
        }
        while(this.joueurs[this.player] == "X");
        
        for(int i=0;i<4;i++) {
            this.home[i].Click += new EventHandler(Choice_Pion_Home);
            this.home[i].Click -= new EventHandler(home_Click);
        }

        for(int i=0;i<56;i++) {
            this.parcours[i].Click += new EventHandler(Choice_Pion_Parcours);
        }

        for(int i=0;i<4;i++) {
            for(int j=0;j<6;j++) {
                this.end[i,j].Click += new EventHandler(Choice_Pion_End);
            }
        }




        this.jouer.Text = "Lancer le Dé";
        this.jouer.Width += this.jouer.Height;
        this.jouer.Left -= this.jouer.Height;

        this.scrore.Width = this.jouer.Width;
        this.scrore.Height = this.jouer.Width;
        this.scrore.Left = this.jouer.Left;
        this.scrore.Top = this.jouer.Top - this.scrore.Height;
        this.scrore.Font = new Font(this.scrore.Font.Name,this.scrore.Width/2);
        this.scrore.FlatStyle = FlatStyle.Flat;
        this.scrore.FlatAppearance.BorderSize = this.scrore.Width/20;
        this.scrore.Enabled = false;

        this.turn.Width = this.jouer.Width;
        this.turn.Height = this.jouer.Width/3;
        this.turn.Left = this.jouer.Left;
        this.turn.Top = this.jouer.Top - this.turn.Height*5;
        this.turn.Font = new Font(this.turn.Font.Name,this.turn.Height/2);
        this.turn.TextAlign = ContentAlignment.MiddleCenter;
        this.turn.Enabled = false;

        switch(this.player) {
            case 0: this.turn.BackColor = Color.Red;
            break;
            case 1: this.turn.BackColor = Color.Green;
            break;
            case 2: this.turn.BackColor = Color.Blue;
            break;
            case 3: this.turn.BackColor = Color.Yellow;
            break;
        }
        this.turn.Text = this.JoueurName[this.player].Text;



        
        this.jouer.Click -= new EventHandler(jouer_Click);
        this.jouer.Click += new EventHandler(Lancer_De);




        for(int i=0;i<4;i++) {
            this.home[i].Text = "";
            this.home[i].Enabled = false;
        }

        

        //Rouge
        if(this.joueurs[0]!="X") {
            for(int i=0;i<2;i++) {
                this.pions[0,i] = new Button();
                this.home[0].Controls.Add(this.pions[0,i]);
                this.pions[0,i].BackColor = Color.Transparent;
                this.pions[0,i].FlatStyle = FlatStyle.Flat;
                this.pions[0,i].Name = "-1";
                this.pions[0,i].FlatAppearance.BorderSize = 0;
                this.pions[0,i].Size = new Size(this.parcours[0].Width-this.parcours[0].FlatAppearance.BorderSize,this.parcours[0].Height-this.parcours[0].FlatAppearance.BorderSize);
                this.pions[0,i].BackgroundImage = Image.FromFile("img/Dada/pionR.png");
                this.pions[0,i].BackgroundImageLayout = ImageLayout.Zoom;
            }
            this.pions[0,0].Location = new Point(this.pions[0,0].Width,this.pions[0,0].Height);
            this.pions[0,1].Location = new Point(this.home[0].Width-this.pions[0,0].Width*2,this.home[0].Height-this.pions[0,0].Height*2);
        }


        //Vert
        if(this.joueurs[1]!="X") {
            for(int i=0;i<2;i++) {
                this.pions[1,i] = new Button();
                this.home[1].Controls.Add(this.pions[1,i]);
                this.pions[1,i].BackColor = Color.Transparent;
                this.pions[1,i].FlatStyle = FlatStyle.Flat;
                this.pions[1,i].Name = "-1";
                this.pions[1,i].FlatAppearance.BorderSize = 0;
                this.pions[1,i].Size = new Size(this.parcours[0].Width-this.parcours[0].FlatAppearance.BorderSize,this.parcours[0].Height-this.parcours[0].FlatAppearance.BorderSize);
                this.pions[1,i].BackgroundImage = Image.FromFile("img/Dada/pionV.png");
                this.pions[1,i].BackgroundImageLayout = ImageLayout.Zoom;
            }
            this.pions[1,0].Location = new Point(this.pions[1,0].Width,this.pions[1,0].Height);
            this.pions[1,1].Location = new Point(this.home[1].Width-this.pions[1,0].Width*2,this.home[1].Height-this.pions[1,0].Height*2);
        }
        

        //Bleu
        if(this.joueurs[2]!="X") {
            for(int i=0;i<2;i++) {
                this.pions[2,i] = new Button();
                this.home[2].Controls.Add(this.pions[2,i]);
                this.pions[2,i].BackColor = Color.Transparent;
                this.pions[2,i].FlatStyle = FlatStyle.Flat;
                this.pions[2,i].Name = "-1";
                this.pions[2,i].FlatAppearance.BorderSize = 0;
                this.pions[2,i].Size = new Size(this.parcours[0].Width-this.parcours[0].FlatAppearance.BorderSize,this.parcours[0].Height-this.parcours[0].FlatAppearance.BorderSize);
                this.pions[2,i].BackgroundImage = Image.FromFile("img/Dada/pionB.png");
                this.pions[2,i].BackgroundImageLayout = ImageLayout.Zoom;
            }
            this.pions[2,0].Location = new Point(this.pions[2,0].Width,this.pions[2,0].Height);
            this.pions[2,1].Location = new Point(this.home[2].Width-this.pions[2,0].Width*2,this.home[2].Height-this.pions[2,0].Height*2);
        }


        //Jaune
        if(this.joueurs[3]!="X") {
            for(int i=0;i<2;i++) {
                this.pions[3,i] = new Button();
                this.home[3].Controls.Add(this.pions[3,i]);
                this.pions[3,i].BackColor = Color.Transparent;
                this.pions[3,i].Name = "-1";
                this.pions[3,i].FlatStyle = FlatStyle.Flat;
                this.pions[3,i].FlatAppearance.BorderSize = 0;
                this.pions[3,i].Size = new Size(this.parcours[0].Width-this.parcours[0].FlatAppearance.BorderSize,this.parcours[0].Height-this.parcours[0].FlatAppearance.BorderSize);
                this.pions[3,i].BackgroundImage = Image.FromFile("img/Dada/pionJ.png");
                this.pions[3,i].BackgroundImageLayout = ImageLayout.Zoom;
            }
            this.pions[3,0].Location = new Point(this.pions[3,0].Width,this.pions[3,0].Height);
            this.pions[3,1].Location = new Point(this.home[3].Width-this.pions[3,0].Width*2,this.home[1].Height-this.pions[3,0].Height*2);
        }
    }
    private void Lancer_De(object sender, EventArgs e) {
        int bloque=0;

        this.de = new Random();
        this.avance = de.Next(1,7);
        this.scrore.Text = ""+this.avance;

        this.GameP.Controls.Remove(this.jouer);
        
        for(int i=0;i<2;i++) {
            if(CanMove(this.pions[this.player,i])) {
                this.pions[this.player,i].Parent.Enabled = true;
                
                this.CasePlay[i] = (Button)this.pions[this.player,i].Parent;
                this.tampon[i] = this.CasePlay[i].FlatAppearance.BorderColor;
                this.CasePlay[i].FlatAppearance.BorderColor = Color.White;
            }
            else {
                bloque ++;
                if(bloque == 2) {
                    this.GameP.Controls.Add(this.jouer);
                    this.jouer.Text = "Suivant";
                    this.jouer.Click -= new EventHandler(Lancer_De);
                    this.jouer.Click += new EventHandler(Suivant);
                }
            }
            
            this.pions[this.player,i].Enabled = false;  
        }
    }
    private void Choice_Pion_Home(object sender, EventArgs e) {
        Button Case = (Button)sender;
        Button sup;

        Case.Enabled = false;
    

            
        for(int i=0;i<4;i++) {
            if(this.parcours[this.player*14].Controls.Contains(this.pions[i,0])) {
                sup = this.pions[i,0];
                this.parcours[this.player*14].Controls.Remove(sup);
                this.home[i].Controls.Add(sup);
                sup.Left = sup.Width;
                sup.Top = sup.Height;
            }
            if(this.parcours[this.player*14].Controls.Contains(this.pions[i,1])) {
                sup = this.pions[i,1];
                this.parcours[this.player*14].Controls.Remove(sup);
                this.home[i].Controls.Add(sup);
                sup.Left = sup.Width*3;
                sup.Top = sup.Height*3;
            }
        }


        if(Case.Controls.Contains(this.pions[this.player,0])) {
            Case.Controls.Remove(this.pions[this.player,0]);
            this.parcours[this.player*14].Controls.Add(this.pions[this.player,0]);
            this.pions[this.player,0].Location = new Point(0,0);
        }
        else {
            Case.Controls.Remove(this.pions[this.player,1]);
            this.parcours[this.player*14].Controls.Add(this.pions[this.player,1]);
            this.pions[this.player,1].Location = new Point(0,0);
        }

        if(this.parcours[this.player*14].Controls.Contains(this.Fake)) {
            this.parcours[this.player*14].Controls.Remove(this.Fake);
        }


        Case.MouseHover -= new EventHandler(Where_Hover_Home);
        Case.MouseLeave -= new EventHandler(Leave_Hover_Home);
        NextPlayer();
    }
    private void Choice_Pion_Parcours(object sender, EventArgs e) {
        int n=-1;
        Button Case = (Button)sender;
        Button tmp, sup;

        Case.Enabled = false;

        tmp = (Button)(Case.GetChildAtPoint(new Point(0,0)));

        Case.Enabled = false;

        Case.Controls.Remove(tmp);

        for(int i=0;i<56;i++) {
            if(Case == this.parcours[i]) {
                n = i;
            }
        }
        if(n == this.player * 14 -1 || (this.player == 0 && n == 55)) {
            this.end[this.player,0].Controls.Add(tmp);

            if(this.end[this.player,0].Controls.Contains(this.Fake)) {
                this.end[this.player,0].Controls.Remove(this.Fake);
            }
        }
        else {
            if(parcours[(n+this.avance)%56].GetChildAtPoint(new Point(0,0)) != null) {
                sup = (Button)parcours[(n+this.avance)%56].GetChildAtPoint(new Point(0,0));
                this.parcours[(n+this.avance)%56].Controls.Remove(sup);
                for(int i=0;i<4;i++) {
                    if(this.pions[i,0] == sup) {
                        this.home[i].Controls.Add(sup);
                        sup.Left = sup.Width;
                        sup.Top = sup.Height;
                    }
                    if(this.pions[i,1] == sup) {
                        this.home[i].Controls.Add(sup);
                        sup.Left = sup.Width*3;
                        sup.Top = sup.Height*3;
                    }
                }      
            }
            this.parcours[(n+this.avance)%56].Controls.Add(tmp);

            if(this.parcours[(n+this.avance)%56].Controls.Contains(this.Fake)) {
                this.parcours[(n+this.avance)%56].Controls.Remove(this.Fake);
            }
        }
        

        Case.MouseHover -= new EventHandler(Where_Hover_Parcours);
        Case.MouseLeave -= new EventHandler(Leave_Hover_Parcours);
        NextPlayer();
    }
    private void Choice_Pion_End(object sender, EventArgs e) {
        Button Case = (Button)sender;
        Button tmp;
        int n=0;


        Case.Enabled = false;


        for(int j=0;j<6;j++) {
            if(Case == this.end[this.player,j]) {
                    n = j;
            }
        }
        tmp = (Button)(Case.GetChildAtPoint(new Point(0,0)));
        Case.Controls.Remove(tmp);

        if(n == 5 && this.avance == 6) {
            tmp.Left = tmp.Width*2;
            tmp.Top = tmp.Height *3*(this.player+1);
                
            if(this.GameP.GetChildAtPoint(new Point(tmp.Width*2,tmp.Height*3*(this.player+1))) != null) {
                this.GameP.Controls.Add(this.winner[this.nbwin]);
                this.winner[this.nbwin].Location = new Point(tmp.Left+tmp.Width*2,tmp.Top);
                this.nbwin++;
                this.joueurs[this.player] = "X";
                testFin();
            }
            this.GameP.Controls.Add(tmp);
        }

        else if(this.avance == n+2) {
            this.end[this.player,n+1].Controls.Add(tmp);
            
            if(this.end[this.player,n+1].Controls.Contains(this.Fake)) {
                this.end[this.player,n+1].Controls.Remove(this.Fake);
            }
        }
        
        Case.MouseHover -= new EventHandler(Where_Hover_End);
        Case.MouseLeave -= new EventHandler(Leave_Hover_End);
        NextPlayer();
    }
    private void Suivant(object sender, EventArgs e) {
        this.jouer.Text = "Lancer le Dé";
        this.jouer.Click += new EventHandler(Lancer_De);
        this.jouer.Click -= new EventHandler(Suivant);

        NextPlayer();
    }
    private void setting_Click(object sender, EventArgs e) {
        this.window.settingWindow.add();
        this.window.settingWindow.Resize_Setting();
    }
    private void more_Click(object sender, EventArgs e) {
        this.More.Location = new Point(this.more.Right-this.More.Width,this.more.Bottom+this.more.Height/20);
        this.GameP.Controls.Add(this.More);
        this.more.Enabled = false;
    }
    private void or_more_Click(object sender, EventArgs e) {
        if(this.GameP.Controls.Contains(this.More)) {
            this.GameP.Controls.Remove(this.More);
            this.more.Enabled = true;
        }
    }


    private void Where_Hover_Home(object sender, EventArgs e) {
        this.parcours[this.player*14].Controls.Add(this.Fake);
    }
    private void Leave_Hover_Home(object sender, EventArgs e) {
        this.parcours[this.player*14].Controls.Remove(this.Fake);
    }
    private void Where_Hover_Parcours(object sender, EventArgs e) {
        Button tmp = (Button)sender;
        int n=-1;
        for(int i=0;i<56;i++) {
            if(this.parcours[i] == tmp) {
                n=i;
            }
        }
        if(n == this.player*14-1 || (this.player == 0 && n == 55)) {
            this.end[this.player,0].Controls.Add(this.Fake);
        }
        else {
            this.parcours[(n+this.avance)%56].Controls.Add(this.Fake);
        }
    }
    private void Leave_Hover_Parcours(object sender, EventArgs e) {
        Button tmp = (Button)sender;
        int n=-1;
        for(int i=0;i<56;i++) {
            if(this.parcours[i] == tmp) {
                n=i;
            }
        }
        if(n == this.player*14-1 || (this.player == 0 && n == 55)) {
            this.end[this.player,0].Controls.Remove(this.Fake);
        }
        else {
            this.parcours[(n+this.avance)%56].Controls.Remove(this.Fake);
        } 
    }
    private void Where_Hover_End(object sender, EventArgs e) {
        Button tmp = (Button)sender;
        int n=-1;
        for(int i=0;i<5;i++) {
            if(this.end[this.player,i] == tmp) {
                n=i;
            }
        }
        this.end[this.player,n+1].Controls.Add(this.Fake);
    }
    private void Leave_Hover_End(object sender, EventArgs e) {
        Button tmp = (Button)sender;
        int n=-1;
        for(int i=0;i<5;i++) {
            if(this.end[this.player,i] == tmp) {
                n=i;
            }
        }
        this.end[this.player,n+1].Controls.Remove(this.Fake);
    }


    private void Init_Partie() {
        this.GameP = new Panel();
        this.parcours = new Button[56];
        this.home = new Button[4];
        this.end = new Button[4,6];
        this.jouer = new Button();
        this.winner = new Button[3];
        this.ColorWin = new Color[3];
        this.tampon = new Color[2];
        this.CasePlay = new Button[2];
        this.Fake = new Button();
        this.more = new Button();
        this.JoueurName = new TextBox[4];


        

        this.CaseSize = new Size(this.window.GameWindow.Width/30,this.window.GameWindow.Width/30);


        this.GameP.Controls.Add(this.more);
        this.GameP.Controls.Add(this.jouer);

        this.GameP.Click += new EventHandler(or_more_Click);


        //Init GameP
        this.GameP.Size = this.window.GameWindow.Size;
        this.GameP.BackColor = Color.LightGray;

        
        //Init ColorWin
        this.ColorWin[0] = new Color();
        this.ColorWin[1] = new Color();
        this.ColorWin[2] = new Color();
    
        this.ColorWin[0] = Color.Gold;
        this.ColorWin[1] = Color.Silver;
        this.ColorWin[2] = Color.Brown;


        //Init Fake
        this.Fake.Size = this.CaseSize;
        this.Fake.BackgroundImage = Image.FromFile("img/Dada/PionFake.png");
        this.Fake.BackgroundImageLayout = ImageLayout.Zoom;
        this.Fake.FlatStyle = FlatStyle.Flat;
        this.Fake.Enabled = false;
        this.Fake.FlatAppearance.BorderSize = 0;
        this.Fake.BackColor = Color.Transparent;


        //Init more
        this.more.Size = this.CaseSize;
        this.more.BackgroundImage = Image.FromFile("img/More.png");
        this.more.BackgroundImageLayout = ImageLayout.Zoom;
        this.more.FlatStyle = FlatStyle.Flat;
        this.more.FlatAppearance.BorderSize = 0;
        this.more.BackColor = Color.Transparent;
        this.more.Left = this.GameP.Width - this.more.Width*2;
        this.more.Top = this.more.Height;
        this.more.Click += new EventHandler(more_Click);


        //Init winner band
        for(int i=0;i<3;i++) {
            this.winner[i] = new Button();
            this.winner[i].Size = this.CaseSize;
            this.winner[i].Width = this.winner[i].Height * 3;
            this.winner[i].FlatStyle = FlatStyle.Flat;
            this.winner[i].FlatAppearance.BorderSize = 0;
            this.winner[i].Enabled = false;
            this.winner[i].BackColor = this.ColorWin[i];
            this.winner[i].Text = "#"+(i+1);
            this.winner[i].Font = new Font(this.winner[i].Font.Name, this.winner[i].Height-this.winner[i].Height/20);
        }
        

        //Init Home and End
        for(int i=0;i<4;i++) {
            this.home[i] = new Button();
            this.home[i].FlatStyle = FlatStyle.Flat;
            this.GameP.Controls.Add(this.home[i]);
            this.home[i].Click += new EventHandler(home_Click);
        }

        Color end;

        for(int i=0;i<4;i++) {
            if(i==0) {
                end = Color.DarkRed;
            }
            else if(i==1) {
                end = Color.DarkGreen;
            }
            else if(i==2) {
                end = Color.DarkBlue;
            }
            else {
                end = Color.Orange;
            }
            
            for(int j=0;j<6;j++) {
                this.end[i,j] = new Button();
                this.end[i,j].BackColor = end;
                this.end[i,j].Enabled = false;
                this.end[i,j].Text = ""+(j+1);
                this.end[i,j].FlatStyle = FlatStyle.Flat;
                this.end[i,j].Size = CaseSize;
                this.end[i,j].FlatAppearance.BorderSize = this.end[i,j].Width/20;
                this.GameP.Controls.Add(this.end[i,j]);
                this.end[i,j].Font = new Font(this.end[i,j].Font.Name, this.end[i,j].Width/2);
            }
        }

        this.home[0].BackColor = Color.Red;
        this.home[0].FlatAppearance.BorderColor = Color.DarkRed;

        this.home[1].BackColor = Color.Green;
        this.home[1].FlatAppearance.BorderColor = Color.DarkGreen;

        this.home[2].BackColor = Color.Blue;
        this.home[2].FlatAppearance.BorderColor = Color.DarkBlue;

        this.home[3].BackColor = Color.Yellow;
        this.home[3].FlatAppearance.BorderColor = Color.Orange;

        

        //Init Parcours
        for(int i=0;i<56;i++) {
            this.parcours[i] = new Button();
            this.parcours[i].Enabled = false;
            this.GameP.Controls.Add(this.parcours[i]);
            this.parcours[i].FlatStyle = FlatStyle.Flat;
            this.parcours[i].Size = CaseSize;
            this.parcours[i].FlatAppearance.BorderSize = this.parcours[i].Width/20;
            if(i<56/4) {
                this.parcours[i].BackColor = Color.Red;
                this.parcours[i].FlatAppearance.BorderColor = Color.DarkRed;
            }
            else if(i<56/2) {
                this.parcours[i].BackColor = Color.Green;
                this.parcours[i].FlatAppearance.BorderColor = Color.DarkGreen;
            }
                else if(i<56/4*3) {
                this.parcours[i].BackColor = Color.Blue;
                this.parcours[i].FlatAppearance.BorderColor = Color.DarkBlue;
            }
            else {
                this.parcours[i].BackColor = Color.Yellow;
                this.parcours[i].FlatAppearance.BorderColor = Color.Orange;
            }  
        }


        //Rouge
        this.parcours[55].Location = new Point(this.GameP.Width/2-this.parcours[0].Width/2,this.GameP.Width/50);
        this.parcours[0].Location = new Point(this.parcours[55].Right,this.parcours[55].Top);
        this.parcours[0].Text = "D";
        this.parcours[0].Font = new Font(this.parcours[0].Font.Name, this.parcours[0].Width/2);


        for(int i=1;i<7;i++) {
            this.parcours[i].Location = new Point(this.parcours[i-1].Left,this.parcours[i-1].Bottom);
        }
        for(int i=7;i<13;i++) {
            this.parcours[i].Location = new Point(this.parcours[i-1].Right,this.parcours[i-1].Top);
        }


        //Vert
        this.parcours[13].Location = new Point(this.parcours[12].Left,this.parcours[12].Bottom);
        this.parcours[14].Location = new Point(this.parcours[13].Left,this.parcours[13].Bottom);
        this.parcours[14].Text = "D";
        this.parcours[14].Font = new Font(this.parcours[14].Font.Name, this.parcours[14].Width/2);


        for(int i=15;i<21;i++) {
            this.parcours[i].Location = new Point(this.parcours[i-1].Left-this.parcours[0].Width,this.parcours[i-1].Top);
        }
        for(int i=21;i<27;i++) {
            this.parcours[i].Location = new Point(this.parcours[i-1].Left,this.parcours[i-1].Bottom);
        }


        //Bleu
        this.parcours[27].Location = new Point(this.parcours[26].Left-this.parcours[0].Width,this.parcours[26].Top);
        this.parcours[28].Location = new Point(this.parcours[27].Left-this.parcours[0].Width,this.parcours[27].Top);
        this.parcours[28].Text = "D";
        this.parcours[28].Font = new Font(this.parcours[28].Font.Name, this.parcours[28].Width/2);


        for(int i=29;i<35;i++) {
            this.parcours[i].Location = new Point(this.parcours[i-1].Left,this.parcours[i-1].Top-this.parcours[0].Height);
        }
        for(int i=35;i<41;i++) {
            this.parcours[i].Location = new Point(this.parcours[i-1].Left-this.parcours[0].Width,this.parcours[i-1].Top);
        }


        //Jaune
        this.parcours[41].Location = new Point(this.parcours[40].Left,this.parcours[40].Top-this.parcours[0].Height);
        this.parcours[42].Location = new Point(this.parcours[41].Left,this.parcours[41].Top-this.parcours[0].Height);
        this.parcours[42].Text = "D";
        this.parcours[42].Font = new Font(this.parcours[42].Font.Name, this.parcours[42].Width/2);

        for(int i=43;i<49;i++) {
            this.parcours[i].Location = new Point(this.parcours[i-1].Right,this.parcours[i-1].Top);
        }
        for(int i=49;i<55;i++) {
            this.parcours[i].Location = new Point(this.parcours[i-1].Left,this.parcours[i-1].Top-this.parcours[0].Height);
        }

        this.home[0].Location = new Point(this.parcours[1].Right+this.parcours[0].Width/2,this.parcours[0].Top);
        this.home[0].Size = new Size(this.parcours[8].Top-this.parcours[0].Top-this.parcours[0].Height/2,this.parcours[8].Top-this.parcours[0].Top-this.parcours[0].Height/2);
        
        this.home[1].Location = new Point(this.parcours[1].Right+this.parcours[0].Width/2,this.parcours[15].Bottom+this.parcours[0].Width/2);
        this.home[1].Size = new Size(this.parcours[8].Top-this.parcours[0].Top-this.parcours[0].Height/2,this.parcours[8].Top-this.parcours[0].Top-this.parcours[0].Height/2);
        
        this.home[2].Location = new Point(this.parcours[41].Left,this.home[1].Top);
        this.home[2].Size = new Size(this.parcours[8].Top-this.parcours[0].Top-this.parcours[0].Height/2,this.parcours[8].Top-this.parcours[0].Top-this.parcours[0].Height/2);
        
        this.home[3].Location = new Point(this.parcours[41].Left,this.parcours[0].Top);
        this.home[3].Size = new Size(this.parcours[8].Top-this.parcours[0].Top-this.parcours[0].Height/2,this.parcours[8].Top-this.parcours[0].Top-this.parcours[0].Height/2);
        


        this.end[0,0].Location = new Point(this.parcours[55].Left,this.parcours[55].Bottom);

        for(int i=1;i<6;i++) {
            this.end[0,i].Location = new Point(this.end[0,i-1].Left,this.end[0,i-1].Bottom);
        }

        this.end[1,0].Location = new Point(this.parcours[12].Left-this.parcours[0].Width,this.parcours[12].Bottom);

        for(int i=1;i<6;i++) {
            this.end[1,i].Location = new Point(this.end[1,i-1].Left-this.parcours[0].Width,this.end[1,i-1].Top);
        }

        this.end[2,0].Location = new Point(this.parcours[27].Left,this.parcours[27].Top-this.parcours[0].Height);

        for(int i=1;i<6;i++) {
            this.end[2,i].Location = new Point(this.end[2,i-1].Left,this.end[2,i-1].Top-this.parcours[0].Height);
        }

        this.end[3,0].Location = new Point(this.parcours[41].Right,this.parcours[41].Top);

        for(int i=1;i<6;i++) {
            this.end[3,i].Location = new Point(this.end[3,i-1].Right,this.end[3,i-1].Top);
        }

        for(int i=0;i<4;i++) {
            this.home[i].FlatAppearance.BorderSize = this.home[i].Width/30;
        }
        
        this.jouer.Size = new Size(this.parcours[0].Width*4,this.parcours[0].Height*2);
        this.jouer.Location = new Point(this.home[1].Left+this.jouer.Width*2, this.end[2,0].Bottom);
        this.jouer.Text = "PLAY";
        this.jouer.BackColor = Color.Gray;
        this.jouer.FlatStyle = FlatStyle.Flat;
        this.jouer.FlatAppearance.BorderSize = this.jouer.Width/10;
        this.jouer.FlatAppearance.BorderColor = Color.LightGray;
        this.jouer.Font = new Font(this.jouer.Font.Name, this.jouer.Width/10);
        this.jouer.Click += new EventHandler(jouer_Click);


        for(int i=0;i<4;i++) {
            this.JoueurName[i] = new TextBox();
            this.JoueurName[i].Size = new Size(this.home[i].Width-this.home[i].Width/10, this.home[i].Height/3);
            this.JoueurName[i].Font = new Font(this.JoueurName[i].Font.Name, this.JoueurName[i].Height);
            this.JoueurName[i].TextAlign = HorizontalAlignment.Center;
            this.JoueurName[i].Location = new Point(this.home[i].Width/2-this.JoueurName[i].Width/2,this.home[i].Height/2-this.JoueurName[i].Height/2);
            switch(i) {
                case 0: this.JoueurName[i].BackColor = Color.Pink; break;
                case 1: this.JoueurName[i].BackColor = Color.LightGreen; break;
                case 2: this.JoueurName[i].BackColor = Color.LightBlue; break;
                case 3: this.JoueurName[i].BackColor = Color.LightYellow; break;
            }
        }

        Init_More();
    }
    private void Init_More() {
        this.exit = new Button();
        this.setting = new Button();
        this.More = new Panel();

        this.More.Controls.Add(this.exit);
        this.More.Controls.Add(this.setting);

        this.More.Size = new Size(this.more.Width*4, this.more.Height*2);
        this.More.Location = new Point(this.more.Right-this.More.Width,(int)(this.more.Bottom*1.1));
        this.More.BackColor = ColorParamButton;
        
        
        this.setting.Text = "Paramètres";
        this.setting.Size = new Size(this.More.Width,this.More.Height/2);
        this.setting.FlatStyle = FlatStyle.Flat;
        this.setting.FlatAppearance.BorderSize = 0;
        this.setting.FlatAppearance.MouseOverBackColor = this.exit.FlatAppearance.CheckedBackColor;
        this.setting.BackColor = ColorParam;
        this.setting.Click += new EventHandler(setting_Click);


        this.exit.Text = "Retourner Au Menu";
        this.exit.Size = new Size(this.More.Width,this.More.Height/2);
        this.exit.Location = new Point(this.setting.Left,this.setting.Bottom);
        this.exit.FlatStyle = FlatStyle.Flat;
        this.exit.FlatAppearance.BorderSize = 0;
        this.exit.FlatAppearance.MouseOverBackColor = this.exit.FlatAppearance.CheckedBackColor;
        this.exit.BackColor = ColorParam;
        this.exit.Click += new EventHandler(Menu_Return);
    }
    private void NextPlayer() {
        this.scrore.Text = "";
        for(int i=0;i<2;i++) {
            if(this.pions[this.player,i].Parent != this.GameP) {
            this.pions[this.player,i].Parent.Enabled = false;
            }
            if(CasePlay[i] != null) {
                CasePlay[i].FlatAppearance.BorderColor = this.tampon[i];
                CasePlay[i] = null;
            }
        }
        switch(this.player) {
            case 0: this.home[this.player].FlatAppearance.BorderColor = Color.DarkRed;      break;
            case 1: this.home[this.player].FlatAppearance.BorderColor = Color.DarkGreen;    break;
            case 2: this.home[this.player].FlatAppearance.BorderColor = Color.DarkBlue;     break;
            case 3: this.home[this.player].FlatAppearance.BorderColor = Color.Orange;       break;
        }


        this.GameP.Controls.Add(this.jouer);
        this.jouer.Enabled = true;

        //this.GameP.Controls.Remove(this.scrore);

        if(this.avance != 6) {
            do {
                this.player+=1;
                this.player%=4;
            }
            while(this.joueurs[this.player] == "X");
            

            switch(this.player) {
                case 0: this.turn.BackColor = Color.Red; break;
                case 1: this.turn.BackColor = Color.Green; break;
                case 2: this.turn.BackColor = Color.Blue; break;
                case 3: this.turn.BackColor = Color.Yellow; break;
            }
            this.turn.Text = this.JoueurName[this.player].Text;
        }
    }
    private void testFin() {
        int n=0;
        for(int i=0;i<4;i++) {
            if(this.joueurs[i]!="X") {
                n++;
            }
        }
        if(n<2) {
            this.GameP.Controls.Remove(this.scrore);
            this.GameP.Controls.Remove(this.turn);
            this.GameP.Controls.Remove(this.jouer);
            this.turn.Text = "Partie Terminé";
            this.turn.BackColor = Color.Transparent;
            this.jouer.Text = "Rejouer";
            this.jouer.Click -= new EventHandler(Lancer_De);
            this.jouer.Click += new EventHandler(Menu_Return);
        }
        
    }
    private bool CanMove(Button pion) {
        int n=-1;

        if(pion.Parent == this.GameP) {
            return false;
        }


        if(pion.Parent == this.home[this.player]) {
            if(this.avance != 6) {
                return false;
            }
            else {
                for(int i=0;i<2;i++) {
                    if(this.parcours[this.player*14].GetChildAtPoint(new Point(0,0)) == this.pions[this.player,i]) {
                        return false;
                    }
                }
            }
            pion.Parent.MouseHover += new EventHandler(Where_Hover_Home);
            pion.Parent.MouseLeave += new EventHandler(Leave_Hover_Home);
            return true;
        }
        
        
        for(int i=0;i<56;i++) {
            if(pion.Parent == this.parcours[i]) {
                n=i;
            }
        }

        if(n != -1) {
            if(n == this.player * 14-1 || (this.player == 0 && n == 55)) {
                if(this.avance == 1 && this.end[this.player,0].GetChildAtPoint(new Point(0,0)) == null) {
                    pion.Parent.MouseHover += new EventHandler(Where_Hover_Parcours);
                    pion.Parent.MouseLeave += new EventHandler(Leave_Hover_Parcours);
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                for(int i=1;i<this.avance;i++) {
                    if(this.parcours[(n+i)%56].GetChildAtPoint(new Point(0,0)) != null || (n+i)%56 == this.player*14-1 || (this.player == 0 && (n+i)%56 == 55)) {
                        return false;
                    }
                }
                for(int i=0;i<2;i++) {
                    if(this.parcours[(n+this.avance)%56].Contains(this.pions[this.player,i])) {
                        return false;
                    }
                }
            }
            pion.Parent.MouseHover += new EventHandler(Where_Hover_Parcours);
            pion.Parent.MouseLeave += new EventHandler(Leave_Hover_Parcours);
            return true;
        }
        
        n=-1;

        for(int i=0;i<6;i++) {
            if(pion.Parent == this.end[this.player,i]) {
                n=i;
            }
        }

        if(n != -1) {
            if(n == 5 && this.avance == 6) {
                return true;
            }
            if(this.avance != n+2) {
                return false;
            }
            else if(this.end[this.player,n+1].GetChildAtPoint(new Point(0,0)) == null) {
                pion.Parent.MouseHover += new EventHandler(Where_Hover_End);
                pion.Parent.MouseLeave += new EventHandler(Leave_Hover_End);
                return true;
            }
            else {
                return false;
            }
        }
        
        
        return true;
    }


    public void Resize_Dada() {
        this.CaseSize = new Size(this.window.GameWindow.Width/30,this.window.GameWindow.Width/30);

        this.GameP.Size = this.window.GameWindow.Size;

        this.Fake.Size = this.CaseSize;

        this.more.Size = this.CaseSize;
        this.more.Left = this.GameP.Width - this.more.Width*2;
        this.more.Top = this.more.Height;

        for(int i=0;i<3;i++) {
            this.winner[i].Size = this.CaseSize;
            this.winner[i].Width = this.winner[i].Height * 3;
            this.winner[i].Font = new Font(this.winner[i].Font.Name, this.winner[i].Height-this.winner[i].Height/20);
        }

        for(int i=0;i<4;i++) {
            for(int j=0;j<6;j++) {
                this.end[i,j].Size = CaseSize;
                this.end[i,j].FlatAppearance.BorderSize = this.end[i,j].Width/20;
                this.end[i,j].Font = new Font(this.end[i,j].Font.Name, this.end[i,j].Width/2);
            }
        }

        
        for(int i=0;i<56;i++) {
            this.parcours[i].Size = CaseSize;
            this.parcours[i].FlatAppearance.BorderSize = this.parcours[i].Width/20;
        }

        this.parcours[55].Location = new Point(this.GameP.Width/2-this.parcours[0].Width/2,this.GameP.Width/50);
        this.parcours[0].Location = new Point(this.parcours[55].Right,this.parcours[55].Top);
        this.parcours[0].Font = new Font(this.parcours[0].Font.Name, this.parcours[0].Width/2);


        for(int i=1;i<7;i++) {
            this.parcours[i].Location = new Point(this.parcours[i-1].Left,this.parcours[i-1].Bottom);
        }
        for(int i=7;i<13;i++) {
            this.parcours[i].Location = new Point(this.parcours[i-1].Right,this.parcours[i-1].Top);
        }

        this.parcours[13].Location = new Point(this.parcours[12].Left,this.parcours[12].Bottom);
        this.parcours[14].Location = new Point(this.parcours[13].Left,this.parcours[13].Bottom);
        this.parcours[14].Font = new Font(this.parcours[14].Font.Name, this.parcours[14].Width/2);


        for(int i=15;i<21;i++) {
            this.parcours[i].Location = new Point(this.parcours[i-1].Left-this.parcours[0].Width,this.parcours[i-1].Top);
        }
        for(int i=21;i<27;i++) {
            this.parcours[i].Location = new Point(this.parcours[i-1].Left,this.parcours[i-1].Bottom);
        }

        this.parcours[27].Location = new Point(this.parcours[26].Left-this.parcours[0].Width,this.parcours[26].Top);
        this.parcours[28].Location = new Point(this.parcours[27].Left-this.parcours[0].Width,this.parcours[27].Top);
        this.parcours[28].Font = new Font(this.parcours[28].Font.Name, this.parcours[28].Width/2);


        for(int i=29;i<35;i++) {
            this.parcours[i].Location = new Point(this.parcours[i-1].Left,this.parcours[i-1].Top-this.parcours[0].Height);
        }
        for(int i=35;i<41;i++) {
            this.parcours[i].Location = new Point(this.parcours[i-1].Left-this.parcours[0].Width,this.parcours[i-1].Top);
        }

        this.parcours[41].Location = new Point(this.parcours[40].Left,this.parcours[40].Top-this.parcours[0].Height);
        this.parcours[42].Location = new Point(this.parcours[41].Left,this.parcours[41].Top-this.parcours[0].Height);
        this.parcours[42].Font = new Font(this.parcours[42].Font.Name, this.parcours[42].Width/2);

        for(int i=43;i<49;i++) {
            this.parcours[i].Location = new Point(this.parcours[i-1].Right,this.parcours[i-1].Top);
        }
        for(int i=49;i<55;i++) {
            this.parcours[i].Location = new Point(this.parcours[i-1].Left,this.parcours[i-1].Top-this.parcours[0].Height);
        }

        this.home[0].Location = new Point(this.parcours[1].Right+this.parcours[0].Width/2,this.parcours[0].Top);
        this.home[0].Size = new Size(this.parcours[8].Top-this.parcours[0].Top-this.parcours[0].Height/2,this.parcours[8].Top-this.parcours[0].Top-this.parcours[0].Height/2);
        
        this.home[1].Location = new Point(this.parcours[1].Right+this.parcours[0].Width/2,this.parcours[15].Bottom+this.parcours[0].Width/2);
        this.home[1].Size = new Size(this.parcours[8].Top-this.parcours[0].Top-this.parcours[0].Height/2,this.parcours[8].Top-this.parcours[0].Top-this.parcours[0].Height/2);
        
        this.home[2].Location = new Point(this.parcours[41].Left,this.home[1].Top);
        this.home[2].Size = new Size(this.parcours[8].Top-this.parcours[0].Top-this.parcours[0].Height/2,this.parcours[8].Top-this.parcours[0].Top-this.parcours[0].Height/2);
        
        this.home[3].Location = new Point(this.parcours[41].Left,this.parcours[0].Top);
        this.home[3].Size = new Size(this.parcours[8].Top-this.parcours[0].Top-this.parcours[0].Height/2,this.parcours[8].Top-this.parcours[0].Top-this.parcours[0].Height/2);
        


        this.end[0,0].Location = new Point(this.parcours[55].Left,this.parcours[55].Bottom);

        for(int i=1;i<6;i++) {
            this.end[0,i].Location = new Point(this.end[0,i-1].Left,this.end[0,i-1].Bottom);
        }

        this.end[1,0].Location = new Point(this.parcours[12].Left-this.parcours[0].Width,this.parcours[12].Bottom);

        for(int i=1;i<6;i++) {
            this.end[1,i].Location = new Point(this.end[1,i-1].Left-this.parcours[0].Width,this.end[1,i-1].Top);
        }

        this.end[2,0].Location = new Point(this.parcours[27].Left,this.parcours[27].Top-this.parcours[0].Height);

        for(int i=1;i<6;i++) {
            this.end[2,i].Location = new Point(this.end[2,i-1].Left,this.end[2,i-1].Top-this.parcours[0].Height);
        }

        this.end[3,0].Location = new Point(this.parcours[41].Right,this.parcours[41].Top);

        for(int i=1;i<6;i++) {
            this.end[3,i].Location = new Point(this.end[3,i-1].Right,this.end[3,i-1].Top);
        }

        for(int i=0;i<4;i++) {
            this.home[i].FlatAppearance.BorderSize = this.home[i].Width/30;
            this.home[i].Font = new Font(this.home[i].Font.Name, this.home[i].Width/10);
        }
        
        this.jouer.Size = new Size(this.parcours[0].Width*4,this.parcours[0].Height*2);
        this.jouer.Location = new Point(this.home[1].Left+this.jouer.Width*2, this.end[2,0].Bottom);
        this.jouer.Font = new Font(this.jouer.Font.Name, this.jouer.Width/10);

        this.More.Size = new Size(this.more.Width*4, this.more.Height*2);
        this.More.Location = new Point(this.more.Right-this.More.Width,(int)(this.more.Bottom*1.1));

        this.setting.Size = new Size(this.More.Width,this.More.Height/2);

        this.exit.Size = new Size(this.More.Width,this.More.Height/2);
        this.exit.Location = new Point(this.setting.Left,this.setting.Bottom);

        for(int i=0;i<4;i++) {
            this.JoueurName[i].Size = new Size(this.home[i].Width-this.home[i].Width/10, this.home[i].Height/3);
            this.JoueurName[i].Font = new Font(this.JoueurName[i].Font.Name, this.JoueurName[i].Height);
            this.JoueurName[i].Location = new Point(this.home[i].Width/2-this.JoueurName[i].Width/2,this.home[i].Height/2-this.JoueurName[i].Height/2);
        }
    }
    public bool IsHere() {
        if(this.window.GameWindow.Controls.Contains(this.GameP)) {
            return true;
        }
        else {
            return false;
        }
    }
    public void add() {
        this.window.GameWindow.Controls.Add(this.GameP);
    }
}