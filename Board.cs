public class Board {
    private static int Tparc = 52;

    private char[] parc;
    private char[,] end;


    public Board() {
        this.parc = InitParc();
        this.end = InitEnd();

    }

    private char[,] InitEnd() {
        char[,] e = new char[4,6];
        for(int i=0;i<4;i++) {
            for(int j=0;j<6;j++) {
                e[i,j] = 'X';
            }
        }
        return e;
    }
    private char[] InitParc() {
        char[] p = new char[Tparc];
        for(int i=0;i<Tparc;i++) {
            p[i] = 'X';
        }
        return p;
    }


    public bool avance(int pion, int add) {
        if(this.parc[pion]=='X') {
            return false;
        }
        return true;
    }

    public bool avanceEnd(int j, int pion, int add) {
        if(this.end[j,pion]=='X') {
            return false;
        }
        return true;
    }
}