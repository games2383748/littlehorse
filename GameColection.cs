using System;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;

public class GameColection {
    private Panel Colection;
    public Dame JeuDame;
    public Dada JeuDada;
    public Chess JeuChess;
    public Morpion jeuMorpion;
    private API window;
    private Button Dame;
    private Button Dada;
    private Button Chess;
    private Button Morpion;


    public GameColection(API window) {
        this.window = window;

        this.JeuDada = new Dada(window);
        this.JeuDame = new Dame(window);
        this.JeuChess = new Chess(window);
        this.jeuMorpion = new Morpion(window);

        Init();
    }




    private void Init() {
        this.Colection = new Panel();
        this.Colection.Size = this.window.GameWindow.Size;

        this.Colection.BackColor = Color.Brown;

        this.Dada = new Button();
        this.Dame = new Button();
        this.Chess = new Button();
        this.Morpion = new Button();

        this.Dada.FlatStyle = FlatStyle.Flat;
        this.Dame.FlatStyle = FlatStyle.Flat;
        this.Dada.FlatAppearance.BorderSize = 0;
        this.Dame.FlatAppearance.BorderSize = 0;
        this.Dada.FlatAppearance.MouseOverBackColor = Color.Transparent;
        this.Dame.FlatAppearance.MouseOverBackColor = Color.Transparent;

        this.Dada.BackgroundImage = Image.FromFile("img/Colection/JeuDada.png");
        this.Dada.BackgroundImageLayout = ImageLayout.Zoom;

        this.Dame.BackgroundImage = Image.FromFile("img/Colection/JeuDame.png");
        this.Dame.BackgroundImageLayout = ImageLayout.Zoom;

        this.Dada.Click += new EventHandler(LaunchDada);
        this.Dame.Click += new EventHandler(LaunchDame);

        this.Colection.Controls.Add(this.Dada);
        this.Colection.Controls.Add(this.Dame);

        this.Chess.Size = this.Dada.Size;
        this.Chess.Location = new Point(this.Dada.Width/2,this.Dada.Height);

        this.Chess.FlatStyle = FlatStyle.Flat;
        this.Chess.FlatAppearance.BorderSize = 0;
        this.Chess.FlatAppearance.MouseOverBackColor = Color.Transparent;

        this.Chess.BackgroundImage = Image.FromFile("img/Colection/JeuChess.png");
        this.Chess.BackgroundImageLayout = ImageLayout.Zoom;

        this.Chess.Click += new EventHandler(LaunchChess);

        this.Morpion.Size = this.Dada.Size;
        this.Morpion.Location = new Point(this.Dada.Width/2,this.Dada.Height);

        this.Morpion.FlatStyle = FlatStyle.Flat;
        this.Morpion.FlatAppearance.BorderSize = 0;
        this.Morpion.FlatAppearance.MouseOverBackColor = Color.Transparent;

        this.Morpion.BackgroundImage = Image.FromFile("img/Colection/JeuMorpion.png");
        this.Morpion.BackgroundImageLayout = ImageLayout.Zoom;

        this.Morpion.Click += new EventHandler(LaunchMorpion);

        this.Colection.Controls.Add(this.Chess);
        this.Colection.Controls.Add(this.Morpion);
    }




    private void LaunchDada(object sender, EventArgs e) {
        this.window.GameWindow.Controls.Remove(this.Colection);
        this.JeuDada.add();
        this.JeuDada.Resize_Dada();
    }
    private void LaunchDame(object sender, EventArgs e) {
        this.window.GameWindow.Controls.Remove(this.Colection);
        this.JeuDame.add();
        this.JeuDame.Resize_Dame();
    }
    private void LaunchChess(object sender, EventArgs e) {
        this.window.GameWindow.Controls.Remove(this.Colection);
        this.JeuChess.add();
        this.JeuChess.Resize_Chess();
    }
    private void LaunchMorpion(object sender, EventArgs e) {
        this.window.GameWindow.Controls.Remove(this.Colection);
        this.jeuMorpion.add();
        this.jeuMorpion.Resize_Morpion();
    }



    public void add() {
        this.window.GameWindow.Controls.Add(this.Colection);
    }

    public void Resize_Colection() {
        this.Colection.Size = this.window.GameWindow.Size;
        
        this.Dada.Size = new Size(this.Colection.Width/4,this.Colection.Height/5);
        this.Dada.Location = new Point(this.Colection.Width/2-this.Dada.Width/2,this.Colection.Height/2-this.Dada.Height*2);

        this.Dame.Size = this.Dada.Size;
        this.Dame.Location = new Point(this.Dada.Right+this.Dada.Width/2,this.Dada.Top);

        this.Chess.Size = this.Dada.Size;
        this.Chess.Location = new Point(this.Dada.Left-this.Dada.Width,this.Dada.Top);

        this.Morpion.Size = this.Dada.Size;
        this.Morpion.Location = new Point(this.Dada.Left,this.Dada.Bottom + this.Dada.Height);
    }
}